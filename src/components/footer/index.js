import React, { Component } from 'react';
import { Image, Text, AsyncStorage, Linking, TouchableOpacityBase } from 'react-native';
import { Button, Footer, FooterTab, Badge, Icon } from 'native-base';
import styles from "./footerComponentStyle";
import { connect } from 'react-redux';
import * as URL from "../../constants/constants";
import { settingIcon, contactIcon, searchIcon, specialIcon, aFrameIcon } from "../../actions/index";
import firebase, { RemoteMessage, Notification, NotificationOpen } from 'react-native-firebase';

import axios from "axios";
class FooterClass extends Component {
	componentWillUnmount = () => {
		// this.onTokenRefreshListener();
		// this.messageListener();
		// this.notificationDisplayedListener();
		// this.notificationListener();
		// this.notificationOpenedListener();
	}
	_checkPermission = async () => {
		const enabled = await firebase.messaging().hasPermission();
		if (enabled) {
			await this._getToken();
		} else {
			this._requestPermission();
		}
	}

	/**
	 * Retrieve the current registration token
	 */
	_getToken = async () => {
		const fcmToken = await firebase.messaging().getToken();
		if (fcmToken) {
			// user has a device token
			// console.log('User have FCM Token ', fcmToken);
			const body = {
				"user_id": `${this.state.getUserId}`,
				"user_email": `${this.state.getUserEmail}`,
				"fcm_key": `${fcmToken}`
			}
			// console.log('body', body)
			axios.post(`${URL.BASE_WP}push_notification`, body, {
			})
				.then((res) => {
					// console.log('new endpoint response', res)
				}).catch((err) => { console.log('pushnotification api err', err) })



			this.setState({
				fcm: fcmToken
			})

		} else {
			// user doesn't have a device token yet
			console.log('User doesn\'t have FCM Token');
		}
	}

	/**
	 * Request permissions
	 */
	_requestPermission = async () => {
		console.log('In request permission');
		try {
			await firebase.messaging().requestPermission();
			// User has authorised
			this._getToken();
		} catch (error) {
			// User has rejected permissions
			console.log('permission rejected');
		}
	}

	/**
	 * Listen for FCM messages
	 */
	_onMessageReceived = () => {

		/**
		 * A message will trigger the onMessage listener when the application receives a message in the foreground.
		 */
		this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
			// Process your message as required
			console.log('RemoteMessage', message);
		});
	}

	/**
	 * Listeners of Notification
	 */
	_onNotificationDisplay = () => {

		/** Triggered when a particular notification has been displayed */
		this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
			// Process your notification as required
			// ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
			console.log('onNotificationDisplayed', notification);
		});

		/** Triggered when a particular notification has been received */
		this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
			// Process your notification as required
			console.log('onNotification', notification);
		});
	}

	/**
	 * Listen for a Notification being opened
	 */
	_onNotificationOpen = async () => {

		/** App in Foreground and background */
		this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
			// Get the action triggered by the notification being opened
			const action = notificationOpen.action;
			// Get information about the notification that was opened
			const notification: Notification = notificationOpen.notification;
			console.log('FG action', action);
			console.log('FG notification', notification);
		});

		/** App Closed */
		const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
		if (notificationOpen) {
			// App was opened by a notification
			// Get the action triggered by the notification being opened
			const action = notificationOpen.action;
			// Get information about the notification that was opened
			const notification: Notification = notificationOpen.notification;
			console.log('Closed action', action);
			console.log('Closed notification', notification);
		}
	}

	setCurrentTab = (tabTitle) => {

		this.setState({
			settingIcon: 0,
			contactIcon: 0,
			searchIcon: 0,
			specialIcon: 0,
			aFrameIcon: 0,
		})

		this.props.settingIcon(0)
		this.props.contactIcon(0)
		this.props.searchIcon(0)
		this.props.specialIcon(0)
		this.props.aFrameIcon(0)

		switch (tabTitle) {
			case 'settingIcon':
				AsyncStorage.setItem('refine_search_warehouse', "");
				AsyncStorage.setItem('refine_search_Available', "");
				this.setState({ settingIcon: 1, getAccessForFirebaseCount: this.state.getAccessForFirebaseCount + 1 })
				this.props.settingIcon(1)
				this.props.navigation.goBack()
				this.state.navigate('Settings', { footer: this.props })
				break;
			case 'contactIcon':
				AsyncStorage.setItem('refine_search_warehouse', "");
				AsyncStorage.setItem('refine_search_Available', "");
				Linking.openURL('https://elements.design/contact/')		
				this.props.searchIcon(1)
				this.setState({
					getAccessForFirebaseCount: this.state.getAccessForFirebaseCount + 1
				})
				break;
			case 'searchIcon':
				AsyncStorage.setItem('refine_search_warehouse', "");
				AsyncStorage.setItem('refine_search_Available', "");
				this.setState({ searchIcon: 1, getAccessForFirebaseCount: this.state.getAccessForFirebaseCount + 1 })
				this.props.navigation.goBack()
				this.state.navigate('HomeScreen', { RefinedSearch: this.props })
				this.props.searchIcon(1)
				break;
			case 'specialIcon':
				AsyncStorage.setItem('refine_search_warehouse', "");
				AsyncStorage.setItem('refine_search_Available', "");
				this.setState({ specialIcon: 1, getAccessForFirebaseCount: this.state.getAccessForFirebaseCount + 1 })
				this.props.specialIcon(1)
				this.props.navigation.goBack()
				this.state.navigate('SpecialInfo', { footerSpecialInfo: this.props, warehouseSetting: this.state.SettingsDefaultWarehouseAsFun, AvailabilitySetting: this.state.AvailableInventoryAs })
				break;
			case 'aFrameIcon':
				AsyncStorage.setItem('refine_search_warehouse', "");
				AsyncStorage.setItem('refine_search_Available', "");
				this.setState({ aFrameIcon: 1, getAccessForFirebaseCount: this.state.getAccessForFirebaseCount + 1 })
				this.props.aFrameIcon(1)
				this.props.navigation.goBack()
				this.state.navigate('AFrame', { footerAframe: this.props, loginInfo: this.state.ElementInventoryloginDetailAframe })
				break;
			default:
				this.setState({ searchIcon: 1 })
		}


	}
	componentDidMount = async () => {

		AsyncStorage.getItem('ElementInventorylogin').then((res) => {
			this.setState({
				getUserEmail: res
			})
		})
		AsyncStorage.getItem('ElementInventoryloginID').then((res) => {
			this.setState({
				getUserId: res
			})
		})
		AsyncStorage.getItem('SettingsDefaultWarehouse').then((value) => {
			if (value == 'locationAll') {
				this.setState({ SettingsDefaultWarehouseAsFun: 'All' })
			}
			else if (value == 'fabstoneMckinney') {
				this.setState({ SettingsDefaultWarehouseAsFun: 'Fabstone Dallas' })
			} else {
				this.setState({ SettingsDefaultWarehouseAsFun: 'Austin Stone Works' })
			}
		}).done();

		AsyncStorage.getItem('AvailableInventoryAsSetting').then((value) => {
			if (value == 'onHand') {
				this.setState({ AvailableInventoryAs: 'on-hand' })
			} else {
				this.setState({ AvailableInventoryAs: 'Available' })
			}
		}).done();

		AsyncStorage.getItem("myKey1").then((value) => {
			this.setState({ "inventoryDataXYZ": value });

		}).done();

		AsyncStorage.getItem("forAframeAsyncStorage").then((value) => {
			value !== null ?
				this.setState({ "demoAsyncData": JSON.parse(value) })
				:
				this.setState({ "demoAsyncData": [] })
		}).done();

		AsyncStorage.getItem("ElementInventorylogin").then((value) => {

			value !== null ?
				this.setState({ ElementInventoryloginDetailAframe: value })
				: this.setState({ ElementInventoryloginDetailAframe: '0' })
		}).done();


	}

	componentWillReceiveProps = async () => {
		const getAccessForFirebaseCount = this.state.getAccessForFirebaseCount++
		const settingIcon = this.props.footerTabChangee.settingIcon
		const searchIcon = this.props.footerTabChangee.searchIcon
		const specialIcon = this.props.footerTabChangee.specialIcon
		if (settingIcon === 1) {
			AsyncStorage.setItem('getFireBaseAccessCount', JSON.stringify(getAccessForFirebaseCount));

		}
		if (searchIcon === 1) {
			AsyncStorage.setItem('getFireBaseAccessCount', JSON.stringify(getAccessForFirebaseCount));

		}
		if (specialIcon === 1) {
			AsyncStorage.setItem('getFireBaseAccessCount', JSON.stringify(getAccessForFirebaseCount));

		}
		this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(async fcmToken => {

			await this._getToken();
		});

		AsyncStorage.getItem("getFireBaseAccessCount").then(async (value) => {
			if (value !== null && value === '2') {
				await this._checkPermission();
				this._onMessageReceived();
				this._onNotificationDisplay();
				this._onNotificationOpen();
				this.setState({ getFirebaseCount: value })
			}
		}).done();
		AsyncStorage.getItem("forAframeAsyncStorage").then((value) => {
			value !== null ?
				this.setState({ "demoAsyncData": JSON.parse(value) })
				:
				this.setState({ "demoAsyncData": [] })
		}).done();
	}


	constructor(props) {
		super(props);
		this.state = {
			settingIcon: 0,
			contactIcon: 0,
			searchIcon: 0,
			specialIcon: 0,
			aFrameIcon: 0,
			inventoryDataXYZ: [],
			navigate: this.props.navigation.navigate,
			demoAsyncData: [],
			ElementInventoryloginDetailFooter: '0',
			ElementInventoryloginDetailNameFooter: '0',
			SettingsDefaultWarehouseAsFun: null,
			AvailableInventoryAs: null,
			ElementInventoryloginDetailAframe: null,
			getAccessForFirebaseCount: 0,
			getFirebaseCount: null,
			getUserEmail: '',
			getUserId: ''
		}
	}

	render() {
		let Aframelength = (this.state.demoAsyncData).length >= 0 ? this.state.demoAsyncData.length : 0
			const { settingIcon, contactIcon, searchIcon, specialIcon, aFrameIcon } = this.props.footerTabChangee;
		return (
			<Footer style={styles.footerContainer}>
				<FooterTab style={styles.footerTabContainer}>
					<Button
						onPress={settingIcon === 0 ? () => this.setCurrentTab("settingIcon") : null}
						vertical
					>
						{settingIcon === 1 ? <Image
							source={require('../../assets/setting-icon-white.png')}
							style={styles.imgIconStyle}
							resizeMode="contain"
						/> : <Image
								source={require('../../assets/setting-icon.png')}
								style={styles.imgIconStyle}
								resizeMode="contain"
							/>}
						{settingIcon === 1 ?
							<Text style={styles.textIconColorWhite}>Settings</Text>
							:
							<Text style={styles.textIconColor}>Settings</Text>
						}
					</Button>

					<Button
						onPress={contactIcon === 0 ? () => this.setCurrentTab("contactIcon") : null}
						vertical
					>
						{contactIcon === 1 ? <Image
							source={require('../../assets/contact-icon-white.png')}
							style={styles.imgIconStyle}
							resizeMode="contain"
						/> : <Image
								source={require('../../assets/contact-icon.png')}
								style={styles.imgIconStyle}
								resizeMode="contain"
							/>}
						{contactIcon === 1 ?
							<Text style={styles.textIconColorWhite}>Contact</Text>
							:
							<Text style={styles.textIconColor}>Contact</Text>
						}
					</Button>

					<Button
						onPress={searchIcon === 0 ? () => this.setCurrentTab("searchIcon") : null}
						vertical
					>
						{searchIcon === 1 ? <Image
							source={require('../../assets/search-icon-white.png')}
							style={styles.imgIconStyle}
							resizeMode="contain"
						/> : <Image
								source={require('../../assets/search-icon.png')}
								style={styles.imgIconStyle}
								resizeMode="contain"
							/>}
						{searchIcon === 1 ?
							<Text style={styles.textIconColorWhite}>Search</Text>
							:
							<Text style={styles.textIconColor}>Search</Text>
						}
					</Button>

					<Button
						onPress={specialIcon === 0 ? () => this.setCurrentTab("specialIcon") : null}
						vertical
					>
						{specialIcon === 1 ? <Image
							source={require('../../assets/specials-white.png')}
							style={styles.imgIconStyle}
							resizeMode="contain"
						/> : <Image
								source={require('../../assets/specials.png')}
								style={styles.imgIconStyle}
								resizeMode="contain"
							/>}
						{specialIcon === 1 ?
							<Text style={styles.textIconColorWhite}>Specials</Text>
							:
							<Text style={styles.textIconColor}>Specials</Text>
						}
					</Button>

					<Button
						badge
						onPress={aFrameIcon === 0 ? () => this.setCurrentTab("aFrameIcon") : null}
						vertical
					>
						{aFrameIcon === 1 ? <Image
							source={require('../../assets/a-frame-icon-white-update.png')}
							style={styles.imgIconStyleAframe}
							resizeMode="contain"
						/> : <Image
								source={require('../../assets/a-frame-icon-update.png')}
								style={styles.imgIconStyleAframe}
								resizeMode="contain"
							/>}

						{aFrameIcon === 1 ?
							<Badge style={styles.badgeAframeStyleWhite} ><Text style={styles.textAframeStyle} >{Aframelength}</Text></Badge>
							:
							<Badge style={styles.badgeAframeStyle} ><Text style={styles.textAframeStyle} >{Aframelength}</Text></Badge>
						}
						{aFrameIcon === 1 ?
							<Text style={styles.textIconColorWhite}>A-Frame</Text>
							:
							<Text style={styles.textIconColor}>A-Frame</Text>
						}
					</Button>

				</FooterTab>
			</Footer>
		);
	}
}
function mapStateToProps(state) {
	return {
		footerTabChangee: state.footerTabChange
	}
}

function mapDispatchToProps(dispatch) {
	return {
		settingIcon: (text) => dispatch(settingIcon(text)),
		contactIcon: (text) => dispatch(contactIcon(text)),
		searchIcon: (text) => dispatch(searchIcon(text)),
		specialIcon: (text) => dispatch(specialIcon(text)),
		aFrameIcon: (text) => dispatch(aFrameIcon(text)),
	}
}

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(FooterClass)