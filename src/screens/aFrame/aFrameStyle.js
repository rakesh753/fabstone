const React = require("react-native");

export default {
	container: {
		backgroundColor: "#fff",
	},
	DateTimeText: {
		color: "#000",
		fontStyle: "italic",
		fontSize: 11,
	},
	highlightedText: {
		color: "#000",
		fontWeight: "bold",
		fontSize: 15,
		paddingLeft: 10,
	},
	list: {
		width: '100%', 
		marginLeft: 0, 
		paddingLeft: 0, 
		paddingRight: 0, 
		marginRight: 0,
	},
	listColGridImage: {
        alignItems: "stretch",
		flex: 1,
        flexDirection: "row",
		height: 150,
		padding: 5,
		paddingLeft: 10,
	},
	listColGridText: {
		height: 150,
		padding: 5,
	},
	listProTitle: {
		fontWeight: "600",
		paddingLeft: 15,
	},
	listText: {
		color: "#000",
		fontSize: 15,
		flex: 1,
		overflow: "hidden",
	},
	productsIconList: {
		height: 25,
		width: 25,
	},
	productsIconListRow: {
		justifyContent: "center",
		alignItems: "center",
		alignContent: "center",
	},
	productsImageList: {
		borderRadius: 3,
        height: 140,
        width: "100%",
	},
	formInput: {
		backgroundColor: "#f5f5f5",
		borderColor: '#ccc',
		borderWidth: 1,
		borderRadius: 5,
		elevation: 5,
		shadowColor: '#ccc',
		shadowOpacity: 10,
		shadowRadius: 5,
	},
	formItem: {
		borderBottomWidth: 0,
		margin: 10,
		backgroundColor: "#f5f5f5",
		elevation: 1,
		borderWidth: 1,
		borderTopWidth: 2,
		borderBottomWidth: 0,
		borderBottomColor: 'transparent',	
	},
	formButtonSub: {
		backgroundColor: "#323232",
		borderRadius: 5,
		margin: 10,
		alignItems: "center",
		justifyContent: "center",
	},
	formButtonSubText: {
		color: "#fff",
		fontSize: 18,
		fontWeight: "600",
	},
};