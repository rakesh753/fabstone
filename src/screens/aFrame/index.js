import React, { Component } from 'react';
import { Dimensions, Modal, Image, Text, View, AsyncStorage, BackHandler, TouchableOpacity, Platform, Linking, ScrollView, TextInput } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Button, Card, CardItem, H3, Icon, Input, Container, Content, List, ListItem, Item } from 'native-base';
import axios from "axios";
import styles from "./aFrameStyle";
import Footer from "../../components/footer/index";
import { connect } from 'react-redux';
import * as URL from "../../constants/constants";
import { settingIcon, contactIcon, searchIcon, specialIcon, aFrameIcon } from "../../actions/index";

const deviceWidth = Dimensions.get("window").width;

String.prototype.capitalize = function () {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

class CollectionProducts extends Component {
	static navigationOptions = ({ navigation }) => ({
		title: "A-Frame",
		headerLeft: <Icon
			name='arrow-back'
			onPress={() => {
				const { params } = navigation.state;
				const footerAframeA = params ? params.footerAframe : null;
				footerAframeA.aFrameIcon(0)
				footerAframeA.searchIcon(1)
				navigation.goBack()
			}}
			style={{ color: '#fff', marginLeft: 10 }}
		/>
	});

	singleProductScreen = (data) => {
		this.props.aFrameIcon(0)
		this.state.InventoryViewSetting == 'photoView' ?
			this.state.navigate("SingleProductSlider", { ProData: JSON.stringify(data), ProDataSKU: data.main.SKU })
			:
			this.state.navigate("SingleProductList", { ProData: JSON.stringify(data), ProDataSKU: data.main.SKU })
	}

	deleteDataFromAframe = (data, rowId) => {
		var that = this
		let array = this.state.demoAsyncData;
		array.splice(rowId, 1);
		const { params } = this.props.navigation.state;
		const footerAframeA = params ? params.footerAframe : null;
		AsyncStorage.setItem('forAframeAsyncStorage', JSON.stringify(array), () => {
			this.props.navigation.setParams({ footerAframe: footerAframeA });	
			that.setState({ demoAsyncData: array })
		});
	}

	dataSyncAframe = () => {
		alert('Product has been synced.')
		this.setState({ 'DeleteButtonRefresh': '1' });
	}

	constructor(props) {
		super(props);
		this.state = {
			navigate: this.props.navigation.navigate,
			inventoryDataXYZ: [],
			demoAsyncData: [],
			DeleteButtonRefresh: '',
			ElementInventoryloginDetailAframe: null,
			ElementInventoryloginDetailNameAframe: null,
			InventoryViewSetting: '0',
			ElementInventoryloginDetailCollectionProduct: '0',
			modalVisible: false,
			nameGuest: "",
			emailGuest: "",
			phoneGuest: "",
			nameGuestError: false,
			emailGuestError: false,
			phoneGuestError: false,
			valueToSend: 0,
			userId: 0,
			userLoginPrice: null,
			isShowList: false,
		}
	}
	componentWillMount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }
	componentWillUnmount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }

	handleBackButtonClick = () => {
		this.props.settingIcon(0)
		this.props.contactIcon(0)
		this.props.searchIcon(1)
		this.props.specialIcon(0)
		this.props.aFrameIcon(0)
	}

	componentDidMount() {

		AsyncStorage.getItem("ElementInventoryloginUser").then((value) => {
			if (value === null || value === undefined || value === "") {
				this.setState({ userLoginPrice: null });
			} else {
				let temp = JSON.parse(value);
				let loginPrice = temp.meta_data.business_type;
				this.setState({ userLoginPrice: loginPrice })
			}
		}).done();

		AsyncStorage.getItem("ViewInventoryAsSetting").then((value) => {
			this.setState({ "InventoryViewSetting": value })
		}).done();

		AsyncStorage.getItem("myKey1").then((value) => {
			this.setState({ "inventoryDataXYZ": value });
		}).done();

		AsyncStorage.getItem("forAframeAsyncStorage").then((value) => {
			value !== null ?

				this.setState({ "demoAsyncData": JSON.parse(value) })
				:
				this.setState({ "demoAsyncData": [] })
		}).done();

		AsyncStorage.getItem("ElementInventorylogin").then((value) => {
			value !== null ?
				this.setState({ ElementInventoryloginDetailAframe: value })
				: this.setState({ ElementInventoryloginDetailAframe: '0' })
		}).done();

		AsyncStorage.getItem("ElementInventorylogin").then((value) => {
			value !== null ?
				this.setState({ "ElementInventoryloginDetailCollectionProduct": value, isShowList: true })
				: this.setState({ "ElementInventoryloginDetailCollectionProduct": '0', isShowList: true })
		}).done();

		AsyncStorage.getItem("ElementInventoryloginID").then((value) => {
			if (value !== null) {
				this.setState({ userId: value })
			}
		}).done();
	}

	_properCase = (arg) => {
		arg = arg.split(' ');
		arg.map((data, index) => {
			arg[index] = data.charAt(0).toUpperCase() + data.slice(1).toLowerCase();
		});
		return arg.join(' ');
	}

	precisionRound = (number) => {
		let no = number.replace("$", "");
		return `$${+(Math.round(no * 100) / 100)}`;
	}

	precisionRoundWithoutPoint = (number) => {
		let no = number.replace("$", "");
		return `$${+(Math.round(Math.trunc(no) * 100) / 100)}`;
	}

	_checkUserCatPrice = (item) => {
		const { userLoginPrice } = this.state;
		let switchKey = userLoginPrice.toLowerCase();
		if (typeof item === 'string') {		
			return "N/A"
		}else{
		switch (switchKey) {
			case "contractor":
				return this.precisionRound(item.ConPrice);
				break;

			case "designer":
				return this.precisionRound(item.DesPrice);
				break;

			case "fabricator":
				return this.precisionRound(item.FabPrice);
				break;

			case "Fabricatorb":
				return this.precisionRound(item.FabPrice);
				break;

			case "retail":
				return this.precisionRound(item.RetPrice);
				break;

			default:
				return this.precisionRound(item.UnitCost);
				break;
		}
	}

	}

	warehouseMap = (loc) => {
		return <Text onPress={() => this.openWarehouseMap(loc)} style={styles.listText}>{this.locShowUser(loc)}</Text>
	}

	locShowUser = (loc) => {
		if (loc.toLowerCase() === "fabricators stone group") {
			return "Fabstone McKinney";
		}
		if (loc.toLowerCase() === "fabstone dallas") {
			return "Elements Dallas";
		}
		return "Other";
	}

	openWarehouseMap = (loc) => {
		if (loc.toLowerCase() === "fabricators stone group") {
			Platform.OS === 'ios' ? Linking.openURL("http://maps.apple.com/?ll=33.220303,-96.6171767") : Linking.openURL('geo:33.220303,-96.6171767');
		}
		if (loc.toLowerCase() === "fabstone dallas") {
			Platform.OS === 'ios' ? Linking.openURL("http://maps.apple.com/?ll=32.8894086,-96.8858619") : Linking.openURL('geo:32.8894086,-96.8858619');
		}
	}

	setModalVisible = (value, rowId) => {
		if (this.state.userId !== '0' && this.state.userId !== 0) {
			this.sendRequestInfoApi(this.state.userId)
		} else {
			this.setState({ modalVisible: value, valueToSend: rowId });
		}
		this.setState({ valueToSend: rowId });
	}

	submitGuestReq = () => {
		this.state.nameGuest === "" ? this.setState({ nameGuestError: true }) : this.setState({ nameGuestError: false });
		this.state.emailGuest === "" ? this.setState({ emailGuestError: true }) : this.setState({ emailGuestError: false });
		this.state.phoneGuest === "" ? this.setState({ phoneGuestError: true }) : this.setState({ phoneGuestError: false });

		if (this.state.nameGuest !== "" && this.state.emailGuest !== "" && this.state.phoneGuest !== "") {
			this.sendRequestInfoApi(0)
			this.setState({ modalVisible: !this.state.modalVisible });
		}
	}

	sendRequestInfoApi = (userId) => {
		let arrValue = this.state.valueToSend;

		const finalData = {
			SKU: this.state.demoAsyncData[arrValue].main.SKU,
			SlabName: this.state.demoAsyncData[arrValue].main.ProductName,
			UserEmail: this.state.emailGuest,
			UserID: userId,
			UserName: this.state.nameGuest,
			UserPhone: this.state.phoneGuest
		}
		const body = JSON.stringify(finalData)
		axios
			.post(`${URL.BASE_WP}elements_reserve_for_me`, body)
			.then(function (response) {
				alert(response.data.result);
			})
			.catch(function (error) {
				console.log(error);
			});
		this.setState({
			valueToSend: 0,
			nameGuest: "",
			emailGuest: "",
			phoneGuest: "",
		})
	}

	render() {
		return (
			<Container style={styles.container}>
				<Content>
					{
						this.state.demoAsyncData.length < 1 && (
							<View style={{ justifyContent: "center", alignContent: "center", alignItems: "center", paddingTop: "45%" }}>
								<Text style={{ textAlign: "center", fontSize: 16, color: "#000", }} >
									Please add a slab to your A-Frame
								</Text>
							</View>
						)
					}

					{
						this.state.demoAsyncData.length < 1 ?
							<View style={{ justifyContent: "center", alignContent: "center", alignItems: "center", paddingTop: "45%" }}>
							</View>
							: null
					}


					{
						this.state.demoAsyncData.length > 0 && this.state.isShowList && (
							<List
								dataArray={this.state.demoAsyncData}
								renderRow={(data, secId, rowId) =>
									<ListItem style={styles.list}>
										<Grid>
											<TouchableOpacity onPress={() => this.singleProductScreen(data)}>
												<Row>
													<H3 style={styles.listProTitle}>{data.main.ProductName.toUpperCase()}</H3>
												</Row>
											</TouchableOpacity>
											<Row>
												<Col style={styles.listColGridImage}>
													<TouchableOpacity style={styles.listColGridImage} onPress={() => this.singleProductScreen(data)}>
														<Image
															style={styles.productsImageList}
															source={{ uri: data.main.item_Filename !== '' ? URL.BaseImgUrl + data.main.item_Filename : URL.FABSTON_LOGO }}
															resizeMode="contain"
														/>
													</TouchableOpacity>
												</Col>
												<Col style={styles.listColGridText}>
													{
														data.main.SKU && (
															<Row>
																<Text style={styles.highlightedText}>SKU: </Text>
																<Text numberOfLines={1} style={styles.listText}>{data.main.SKU}</Text>
															</Row>
														)
													}
													{
														data.main.Type && (
															<Row>
																<Text style={styles.highlightedText}>Type: </Text>
																<Text numberOfLines={1} style={styles.listText}>{data.main.Type === "" ? "N/A" : data.main.Type}</Text>
															</Row>
														)
													}
													{
														<Row>
															<Text style={styles.highlightedText}>Color: </Text>
															<Text numberOfLines={1} style={styles.listText}>{data.main.Item_BaseColor === "" ? 'N/A' : data.main.Item_BaseColor}</Text>
														</Row>
													}
													{
														<Row>
															<Text style={styles.highlightedText}>Series: </Text>
															<Text numberOfLines={1} style={styles.listText}>{this._properCase(data.main.ProductGroup)}</Text>
														</Row>
													}
													{
														data.main.UnitCost && this.state.userLoginPrice === null && (
															<Row>
																<Text style={styles.highlightedText}>Price: </Text>
																<Text numberOfLines={1} style={styles.listText}>{this.state.ElementInventoryloginDetailCollectionProduct !== '0' ? this.precisionRound(data.main.UnitCost) : (this.precisionRoundWithoutPoint(data.main.UnitCost).replace('$', "")).replace(/[0-9]/g, "$")}</Text>
															</Row>
														)
													}
													{
														this.state.userLoginPrice !== null &&
														<Row>
															<Text style={styles.highlightedText}>Price: </Text>
															<Text numberOfLines={1} style={styles.listText}>{this.state.ElementInventoryloginDetailCollectionProduct !== '0' ? this._checkUserCatPrice(data.bundles[0]) : (this.precisionRoundWithoutPoint(data.main.UnitCost).replace('$', "")).replace(/[0-9]/g, "$")}</Text>
														</Row>

													}
												</Col>
											</Row>

											<Row>
												<Col size={30}>
													<Row style={styles.productsIconListRow}>
														<View style={{ justifyContent: "center", alignContent: "center", padding: 3 }}>
															<Button transparent block onPress={() => { this.setModalVisible(true, rowId) }}>
																<View style={{ justifyContent: "center", alignContent: "center", alignItems: "center", }}>
																	<Text style={{ textAlign: "center", fontWeight: "600", backgroundColor: "#B08D58", color: "#fff", padding: 5 }}>
																		Reserve For Me
																			</Text>
																</View>
															</Button>
														</View>
													</Row>
												</Col>
												<Col size={30}>
													<Row style={styles.productsIconListRow}>
														<Col>
															<Button transparent block onPress={() => this.dataSyncAframe()}>
																<Image
																	style={styles.productsIconList}
																	source={require('../../assets/reload-icon.png')}
																	resizeMode="contain"
																/>
															</Button>
														</Col>
														<Col>
															<Button transparent block onPress={() => this.singleProductScreen(data)}>
																<Image
																	style={styles.productsIconList}
																	source={require('../../assets/Clip-Group.png')}
																	resizeMode="contain"
																/>
															</Button>
														</Col>
														<Col>
															<Button transparent block onPress={() => this.deleteDataFromAframe(data, rowId)}>
																<Image
																	style={styles.productsIconList}
																	source={require('../../assets/delete.png')}
																	resizeMode="contain"
																/>
															</Button>
														</Col>
													</Row>
												</Col>
											</Row>
										</Grid>
									</ListItem>}
							>
							</List>
						)
					}
					<Modal
						animationType="slide"
						transparent={true}
						visible={this.state.modalVisible}
						onRequestClose={() => {
							alert('Modal has been closed.');
						}}>
						<View style={{ backgroundColor: "rgba(0,0,0,0.5)", flex: 1 }}>
							<View style={{ marginTop: deviceWidth / 3, }}>
								<View style={{ justifyContent: "center", alignContent: "center", alignItems: "center" }} >
									<View style={{ backgroundColor: "#fff", maxHeight: 400, width: deviceWidth - 50, zIndex: 1, }}>
										<Button block transparent style={{ position: "absolute", top: 5, right: 15, zIndex: 2, }} onPress={() => this.setState({ modalVisible: !this.state.modalVisible })}>
											<Image
												style={{ width: 20, height: 20 }}
												source={require('../../assets/cancle_cross.png')}
												resizeMode="contain"
											/>
										</Button>
										<View style={{ paddingTop: 40 }}>
											<ScrollView>
												<View style={{ paddingLeft: 15, paddingRight: 15 }}>
													<Item style={styles.formItem}>
														<Input
															placeholder="Name"
															onChangeText={(nameGuest) => { this.setState({ nameGuest }); }}
															value={this.state.nameGuest}
															style={styles.formInput}
														/>
													</Item>
													{
														this.state.nameGuestError && (<Text style={{ color: "red", fontSize: 15 }} > Name can't be blank </Text>)
													}
													<Item style={styles.formItem}>
														<Input
															placeholder="Email"
															keyboardType="email-address"
															onChangeText={(emailGuest) => { this.setState({ emailGuest }); }}
															value={this.state.emailGuest}
															style={styles.formInput}
														/>
													</Item>
													{
														this.state.emailGuestError && (<Text style={{ color: "red", fontSize: 15 }} > Email can't be blank </Text>)
													}
													<Item style={styles.formItem}>
														<Input
															placeholder="Phone"
															keyboardType="numeric"
															onChangeText={(phoneGuest) => { this.setState({ phoneGuest }); }}
															value={this.state.phoneGuest}
															style={styles.formInput}
														/>
													</Item>
													{
														this.state.phoneGuestError && (<Text style={{ color: "red", fontSize: 15 }} > Phone number can't be blank </Text>)
													}
													<View style={{ marginTop: 10 }} >
														<View style={{ justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
															<Button transparent block style={styles.formButtonSub} onPress={() => this.submitGuestReq()}>
																<Text style={styles.formButtonSubText}>Send</Text>
															</Button>
														</View>
													</View>
												</View>
											</ScrollView>
										</View>
									</View>
								</View>
							</View>
						</View>
					</Modal>
				</Content>
				<Footer navigation={this.props.navigation} />
			</Container>
		);
	}
}
function mapStateToProps(state) {
	return {
		footerTabChangee: state.footerTabChange
	}
}
function mapDispatchToProps(dispatch) {
	return {
		settingIcon: (text) => dispatch(settingIcon(text)),
		contactIcon: (text) => dispatch(contactIcon(text)),
		searchIcon: (text) => dispatch(searchIcon(text)),
		specialIcon: (text) => dispatch(specialIcon(text)),
		aFrameIcon: (text) => dispatch(aFrameIcon(text)),
	}
}
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CollectionProducts)