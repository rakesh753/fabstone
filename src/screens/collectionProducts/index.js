import React, { Component } from 'react';
import { Image, Text, View, ActivityIndicator, AsyncStorage, Share, TouchableOpacity, BackHandler, Platform, Linking, Alert } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Button, Card, CardItem, H3, Icon, Container, Content, List, ListItem } from 'native-base';
import styles from "./collectionProductsStyle";
import Footer from "../../components/footer/index";
import * as URL from "../../constants/constants";
import axios from "axios";
import { connect } from 'react-redux';
import { settingIcon, contactIcon, searchIcon, specialIcon, aFrameIcon } from "../../actions/index";

String.prototype.capitalize = function () {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

class CollectionProducts extends Component {

	static navigationOptions = ({ navigation }) => ({
		title: `${navigation.state.params.title}`,
		headerLeft: <Icon
			name='arrow-back'
			onPress={() => navigation.state.params.increaseCount()}
			style={{ color: '#fff', marginLeft: 10 }}
		/>
	});



	constructor(props) {
		super(props);
		this.state = {
			navigate: this.props.navigation.navigate,
			categoryProductVar: [],
			isLoading: true,
			demoAsync: [],
			ElementInventoryloginDetailCollectionProduct: '0',
			ElementInventoryloginDetailNameCollectionProduct: '0',
			AvailableInventoryAsSetting: null,
			SettingsDefaultWarehouseAsFun: null,
			userLoginPrice: null,
			c: 0,
			showSeries: 'Disco',
			page: 1,
			isShowLoadMoreLoader: false,
			isShowMore: true,
		}
	}

	componentWillMount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }
	componentWillUnmount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }

	handleBackButtonClick = () => {
		this.props.settingIcon(0)
		this.props.contactIcon(0)
		this.props.searchIcon(1)
		this.props.specialIcon(0)
		this.props.aFrameIcon(0)
	}


	precisionRound = (number) => {
		let no = number.replace("$", "");
		return `$${+(Math.round(no * 100) / 100)}`;
	}

	precisionRoundWithoutPoint = (number) => {
		let no = number.replace("$", "");
		return `$${+(Math.round(Math.trunc(no) * 100) / 100)}`;
	}

	_checkUserCatPrice = (item) => {
		const { userLoginPrice } = this.state;
		let switchKey = userLoginPrice.toLowerCase();
		switch (switchKey) {
			case "contractor":
				return this.precisionRound(item.ConPrice);
				break;

			case "designer":
				return this.precisionRound(item.DesPrice);
				break;

			case "fabricator":
				return this.precisionRound(item.FabPrice);
				break;

			case "Fabricatorb":
				return this.precisionRound(item.FabPrice);
				break;

			case "retail":
				return this.precisionRound(item.RetPrice);
				break;

			default:
				return this.precisionRound(item.TL_UnitPrice);
				break;
		}
	}


	_showLoadMoreLoader() {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<ActivityIndicator size="large" color="#323232" />
			</View>
		);
	}

	_showLoadMore() {
		return (
			<View style={{ backgroundColor: "#fff", justifyContent: "center", alignContent: "center", alignItems: "center", paddingBottom: 20, paddingTop: 20 }}>
				<TouchableOpacity onPress={() => this._loadMore()}>
					<View>
						<Text style={{ paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, color: "#b08d58", fontSize: 18, }}>
							Load More
					</Text>
					</View>
				</TouchableOpacity>
			</View>
		)
	}
	_loadMore = () => {
		let that = this;
		that.setState({
			page: this.state.page + 1,
			isShowLoadMoreLoader: true
		}, () => {

			let finalData = {
				"category": this.state.categoryName,
				"warehouse": this.state.SettingsDefaultWarehouseAsFun,
				"page": this.state.page
			}
			axios
				.post(`https://elements.design/wp-json/elements_routes/inventory_by_category_listing`, finalData, {
				})
				.then(function (response) {
					if (response.data.status === 1) {
						let initalRenderTile = response.data.result;
						let mergeArr = that.state.categoryProductVar.concat(initalRenderTile);
						that.setState({ isShowLoadMoreLoader: false, 'categoryProductVar': mergeArr, });
					} else {
						alert("No More Products to load");
						that.setState({ isShowLoadMoreLoader: false, isShowMore: false });
					}
				})
				.catch(function (error) {
					that.setState({ isShowLoadMoreLoader: false })
					console.log(error);
				})
		});



	}
	_increaseCount = () => {
		this.props.searchIcon(1);
		this.props.navigation.goBack();
	}


	componentDidMount = () => {
		this.props.navigation.setParams({ increaseCount: this._increaseCount });

		var that = this
		const { params } = this.props.navigation.state;
		let cat_Name = params ? params.title : null;
		that.setState({
			categoryName: cat_Name

		})



		AsyncStorage.getItem("ElementInventoryloginUser").then((value) => {
			if (value === null || value === undefined || value === "") {
				this.setState({ userLoginPrice: null });
			} else {
				let temp = JSON.parse(value);
				let loginPrice = temp.meta_data.business_type;
				this.setState({ userLoginPrice: loginPrice })
			}
		}).done();

		AsyncStorage.getItem('SettingsDefaultWarehouse').then(async (value) => {
			this.setState({ SettingsDefaultWarehouseAsFun: value })
		}).then(res => {

			AsyncStorage.getItem('AvailableInventoryAsSetting').then(async (value) => {
				this.setState({ AvailableInventoryAsSetting: value })

			}).then(res => {

				let finalData = {
					"category": cat_Name,
					"warehouse": this.state.SettingsDefaultWarehouseAsFun,
					"page": this.state.page
				}
				axios
					.post(`https://elements.design/wp-json/elements_routes/inventory_by_category_listing`, finalData, {

					})
					.then(function (response) {						
						if (response.data.status === 1) {
							let initalRenderTile = response.data.result
							that.setState({ isLoading: false, 'categoryProductVar': initalRenderTile, });
						} else {
							that.setState({ isLoading: false, categoryProductVar: [], });
						}
					})
					.catch(function (error) {
						that.setState({ isLoading: false })
						console.log(error);
					})
			});
		})

		AsyncStorage.getItem("forAframeAsyncStorage").then((value) => {
			this.setState({ "demoAsync": value });
		}).done();

		AsyncStorage.getItem("ElementInventorylogin").then((value) => {
			value !== null ?
				this.setState({ "ElementInventoryloginDetailCollectionProduct": value })
				: this.setState({ "ElementInventoryloginDetailCollectionProduct": '0' })
		}).done();

		AsyncStorage.getItem("ViewInventoryAsSetting").then((value) => {
			this.setState({ "InventoryViewSetting": value })
		}).done();


	}

	singleProductScreen = (data) => {
		this.state.InventoryViewSetting == 'photoView' ?
			this.state.navigate("SingleProductSlider", { ProData: JSON.stringify(data), ProDataSKU: data.main.SKU })
			:
			this.state.navigate("SingleProductList", { ProData: JSON.stringify(data), ProDataSKU: data.main.SKU })
	}
	shareToSocialSites = (data) => {
		let finalData = {
			"sku": data.main.SKU,
			"warehouse": this.state.SettingsDefaultWarehouseAsFun,
		}
		axios
			.post(`${URL.URL_CATEGORY}`, finalData, {})
			.then((res) => {
				if (res.data.status === 1) {
					let data = res.data.result[0]
					let msgString = "Name: " + data.main.ProductName.toUpperCase() + "\n" + "Type: " + data.main.Type + "\n" + "Series: " + data.main.ProductGroup + "\n" + "SKU: " + data.main.SKU + "\n\n";

					data.bundles !== "Record not found!" && data.bundles.map((val, index) => {
						msgString += "Bundle Number: " + val.Bundle + "\nBlock Number: " + val.Block + "\nAvg Size: " + val.Dimensions + "\" \n" + "On Hand: " + val.PCOnHand + "\nAvailable: " + val.CurrentAvailabeQty + "\nLocation: " + this.locShowUser(val.LocationName) + "\n\n";
					});

					let ImgUrl = ''
					if (data.main.item_Filename !== "" && data.main.item_Filename !== null) {
						ImgUrl = URL.BaseImgUrl + data.main.item_Filename;
					} else {
						ImgUrl = URL.FABSTON_LOGO;
					}


					msgString += ImgUrl;

					Share.share(
						{
							message: msgString,
						}).then(result => console.log(result)).catch(errorMsg => console.log(errorMsg));
				}
				Alert.alert('Please retry after some time')
			}).catch((err) => {
				console.log('err', err)
			})
	}

	_properCase = (arg) => {
		arg = arg.split(' ');
		arg.map((data, index) => {
			arg[index] = data.charAt(0).toUpperCase() + data.slice(1).toLowerCase();
		});
		return arg.join(' ');
	}

	addToAframe = (item) => {
		let finalData = {
			"sku": item.main.SKU,
			"warehouse": this.state.SettingsDefaultWarehouseAsFun,
		}
		axios
			.post(`${URL.URL_CATEGORY}`, finalData, {})
			.then((res) => {
				if (res.data.status === 1) {
					let data = res.data.result[0]
					if ((this.state.demoAsync !== null) && (this.state.demoAsync !== '') && (JSON.parse(this.state.demoAsync).length > 0)) {
						let arrdemo = [];
						let arrdemoNew = [];
						let convParse = JSON.parse(this.state.demoAsync)
						convParse.map((asyncData, i) => {
							if (asyncData.main.SKU !== data.main.SKU) {
								let pushdemo = arrdemo.push(asyncData)
							}
							let pushdemo = arrdemoNew.push(asyncData)
						})
						let pushdemo1 = arrdemo.push(data)
						AsyncStorage.setItem('forAframeAsyncStorage', JSON.stringify(arrdemo), () => {
							if (arrdemoNew.length === arrdemo.length) {
								alert('Already added')
							} else {
								alert(data.main.ProductName.toUpperCase() + ' Added to A-Frame')

							}
							this.setState({ demoAsync: JSON.stringify(arrdemo), c: this.state.c + 1 });
						});
					} else {
						let arrdemo = [];
						let pushdemo1 = arrdemo.push(data)
						AsyncStorage.setItem('forAframeAsyncStorage', JSON.stringify(arrdemo), () => {
							alert(data.main.ProductName.toUpperCase() + ' Added to A-Frame')
							this.setState({ demoAsync: JSON.stringify(arrdemo), c: this.state.c + 1 });
						});
					}
				}
			}).catch((err) => {
				console.log('a frame', err)
			})
	}

	warehouseMap = (loc) => {
		return <Text onPress={() => this.openWarehouseMap(loc)} style={styles.listText}>{this.locShowUser(loc)}</Text>
	}

	locShowUser = (loc) => {
		if (loc.toLowerCase() === "fabricators stone group") {
			return "Fabstone McKinney";
		}
		if (loc.toLowerCase() === "fabstone dallas") {
			return "Elements Dallas";
		}
		return "Other";
	}

	openWarehouseMap = (loc) => {
		if (loc.toLowerCase() === "fabricators stone group") {
			Platform.OS === 'ios' ? Linking.openURL("http://maps.apple.com/?ll=33.220303,-96.6171767") : Linking.openURL('geo:33.220303,-96.6171767');
		}
		if (loc.toLowerCase() === "fabstone dallas") {
			Platform.OS === 'ios' ? Linking.openURL("http://maps.apple.com/?ll=32.8894086,-96.8858619") : Linking.openURL('geo:32.8894086,-96.8858619');
		}
	}

	_totalOnHandAvailable = (arr, arg) => {
		let total = 0;
		arr.map((d, i) => {
			total += Number(d[arg]);
		});
		return total;
	}

	render() {
		if (this.state.isLoading) {
			return (
				<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
					<ActivityIndicator size="large" color="#323232" />
				</View>
			);
		}

		return (
			<Container style={styles.container} >
				<Content>
					{this.state.categoryProductVar && this.state.categoryProductVar.length > 0 ?
						(
							<View>
								<List
									dataArray={this.state.categoryProductVar}
									renderRow={(data) =>
										<ListItem style={styles.list}>
											<Grid>
												<TouchableOpacity onPress={() => this.singleProductScreen(data)} >
													<Row>
														<H3 style={styles.listProTitle}>{data.main.ProductName.toUpperCase()}</H3>
													</Row>
												</TouchableOpacity>
												<Row>
													<Col style={styles.listColGridImage}>
														<TouchableOpacity style={styles.listColGridImage} onPress={() => this.singleProductScreen(data)} >
															<Image
																style={styles.productsImageList}
																source={{ uri: data.main.item_Filename !== '' ? URL.BaseImgUrl + data.main.item_Filename : URL.FABSTON_LOGO }}
																resizeMode="contain"
															/>
														</TouchableOpacity>
													</Col>
													<Col style={styles.listColGridText}>

														{
															data.main.SKU && (
																<Row>
																	<Text style={styles.highlightedText}>SKU: </Text>
																	<Text numberOfLines={1} style={styles.listText}>{data.main.SKU}</Text>
																</Row>
															)
														}
														{
															data.main.Type && (
																<Row>
																	<Text style={styles.highlightedText}>Type: </Text>
																	<Text numberOfLines={1} style={styles.listText}>{data.main.Type === "" ? "N/A" : data.main.Type}</Text>
																</Row>
															)
														}
														{

															<Row>
																<Text style={styles.highlightedText}>Color: </Text>
																<Text numberOfLines={1} style={styles.listText}>{data.main.Item_BaseColor}</Text>
															</Row>

														}

														{

															<Row>
																<Text style={styles.highlightedText}>Series: </Text>
																<Text numberOfLines={1} style={styles.listText}>{this._properCase(data.main.ProductGroup)}</Text>
															</Row>

														}
														{
															data.main.UnitCost && this.state.userLoginPrice === null && (
																<Row>
																	<Text style={styles.highlightedText}>Price: </Text>
																	<Text numberOfLines={1} style={styles.listText}>{this.state.ElementInventoryloginDetailCollectionProduct !== '0' ? this.precisionRound(data.main.UnitCost) : (this.precisionRoundWithoutPoint(data.main.UnitCost).replace('$', "")).replace(/[0-9]/g, "$")}</Text>
																</Row>
															)
														}

														{
															this.state.userLoginPrice !== null &&
															<Row>
																<Text style={styles.highlightedText}>Price: </Text>
																<Text numberOfLines={1} style={styles.listText}>{this.state.ElementInventoryloginDetailCollectionProduct !== '0' ? this._checkUserCatPrice(data.main) : (this.precisionRoundWithoutPoint(data.main.UnitCost).replace('$', "")).replace(/[0-9]/g, "$")}</Text>
															</Row>

														}


														<Row>
															<Col>
																<Row style={styles.productsIconListRow}>
																	<Col style={styles.productsIconListCol} >
																		<Button transparent block onPress={() => this.addToAframe(data)}>
																			<Image
																				style={styles.productsIconList}
																				source={require('../../assets/a-icon-update.png')}
																				resizeMode="contain"
																			/>
																		</Button>
																	</Col>
																	<Col style={styles.productsIconListCol} >
																		<Button transparent block onPress={() => this.singleProductScreen(data)} >
																			<Image
																				style={styles.productsIconList}
																				source={require('../../assets/Clip-Group.png')}
																				resizeMode="contain"
																			/>
																		</Button>
																	</Col>
																	<Col style={styles.productsIconListCol} >
																		<Button transparent block onPress={() => this.shareToSocialSites(data)}>
																			<Image
																				style={styles.productsIconList}
																				source={require('../../assets/share.png')}
																				resizeMode="contain"
																			/>
																		</Button>
																	</Col>
																</Row>
															</Col>
														</Row>
													</Col>
												</Row>
											</Grid>
										</ListItem>}
								>
								</List>

								{this.state.categoryProductVar.length > 9 ?
									this.state.isShowLoadMoreLoader === true ? this._showLoadMoreLoader() : this.state.isShowMore === true ? this._showLoadMore() : null : null}

							</View>
						) :
						(<View><Text style={{ fontSize: 20, padding: 20 }}>No Products Available.</Text></View>)
					}
				</Content>

				<Footer navigation={this.props.navigation} c={this.state.c} />
			</Container>
		);
	}

}



function mapStateToProps(state) {
	return {
		footerTabChangee: state.footerTabChange
	}
}
function mapDispatchToProps(dispatch) {
	return {
		settingIcon: (text) => dispatch(settingIcon(text)),
		contactIcon: (text) => dispatch(contactIcon(text)),
		searchIcon: (text) => dispatch(searchIcon(text)),
		specialIcon: (text) => dispatch(specialIcon(text)),
		aFrameIcon: (text) => dispatch(aFrameIcon(text)),
	}
}
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CollectionProducts)