import React, { Component } from 'react';
import { Image, View, ScrollView, Dimensions } from 'react-native';
import { Button, Container, Header, Right } from 'native-base';
import styles from './enlargeImageStyle';
import ZoomableImage from './ZoomableImage';

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

export default class EnlargeImage extends Component {

  cancleCross = () => {
    this.props.navigation.goBack()
  }

  render() {
    const { state } = this.props.navigation;
    let ProImg = state.params ? state.params.img : "<undefined>";
    return (
      <Container style={styles.container} >
        <Header androidStatusBarColor="#323232" style={styles.header}>
          <Right>
            <Button block transparent onPress={() => this.cancleCross()}>
              <Image
                style={styles.imgCancleIconStyle}
                source={require('../../assets/cancle_cross.png')}
                resizeMode="contain"
              />
            </Button>
          </Right>
        </Header>      
        <ZoomableImage
          style={styles.mainView}
          source={ProImg}
          imageWidth={deviceWidth}
          imageHeight={deviceHeight/2}
        />       
      </Container>
    );
  }
}