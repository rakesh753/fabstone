import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { Button, Container, Content, Form, Input, Item, } from 'native-base';
import styles from "./style";
import BannerHeader from "../../components/bannerHeader/index";
import Footer from "../../components/footer/index";
import * as URL from "../../constants/constants";
import axios from "axios";

export default class ResetPswd extends Component {

    static navigationOptions = {
        title: "Reset Password",
    };

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            error: false,
            isLoader: false,
        }
    }

    _forgotPswd = () => {
        if (this.state.email.length > 0) {
            this.setState({ error: false, isLoader: true })
            var that = this;

            axios
                .post(`${URL.BASE_WP}forgot_password`, {
                    user_email: this.state.email,
                })
                .then(function (response) {
                    if (response.data.error) {
                        that.setState({ isLoader: false })
                        alert(response.data.error);
                    } else {
                        that.setState({ isLoader: false })
                        alert(response.data.response_msg);
                    }
                })
                .catch(function (error) {
                    that.setState({ isLoader: false })
                    console.log(error);
                });

        } else {
            this.setState({ error: true })
        }
    }

    render() {
        const { isLoader } = this.state;
        return (
            <Container>
                <Content>
                    <View style={styles.container} >
                        <BannerHeader />
                        <Form style={styles.formContainer}>
                            <Item style={styles.formItem}>
                                <Input
                                    keyboardType={'email-address'}
                                    returnKeyType={'done'}
                                    autoCapitalize={'none'}
                                    placeholder="Email"
                                    type="email"
                                    onChangeText={(email) => { this.setState({ email }); }}
                                    value={this.state.email}
                                    style={styles.formInput}
                                />
                            </Item>
                            <Item>
                                {
                                    this.state.error && <Text style={{ color: "red" }}>Cannot be blank.</Text>
                                }
                            </Item>
                            <Button style={styles.formButtonSub} onPress={() => this._forgotPswd()}>
                                <Text style={styles.formButtonSubText}>Reset Password</Text>
                            </Button>
                            {
                                isLoader && (
                                    <View style={{ flex: 1 }}>
                                        <ActivityIndicator size="large" color="#323232" />
                                    </View>
                                )
                            }
                        </Form>
                    </View>
                </Content>

                <Footer navigation={this.props.navigation} />
            </Container>
        );
    }
}