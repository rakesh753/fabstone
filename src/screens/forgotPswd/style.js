const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceWidth = Dimensions.get("window").width;
export default {
    container: {
        alignItems: "center",
        backgroundColor: "#fff",
        justifyContent: "center",
    },
    formButtonSub: {
        backgroundColor: "#323232",
        borderRadius: 5,
        margin: 10,
        width: deviceWidth - 70,
        alignItems: "center",
        justifyContent: "center",
    },
    formButtonSubText: {
        color: "#fff",
        fontSize: 18,
        fontWeight: "600",
    },
    formContainer: {
        marginTop: 5,
        width: deviceWidth - 50,
    },
    formInput: {
        backgroundColor: "#f5f5f5",
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        elevation: 5,
        shadowColor: '#ccc',
        shadowOpacity: 10,
        shadowRadius: 5,
    },
    formItem: {
        borderBottomWidth: 0,
        margin: 10,
        backgroundColor: "#f5f5f5",
        elevation: 1,
        borderWidth: 1,
        borderTopWidth: 2,
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
    },    
};