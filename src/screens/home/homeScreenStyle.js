const React = require("react-native");
const { Dimensions} = React;


const deviceWidth = Dimensions.get("window").width;


export default {
    buttonSearchStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        width: deviceWidth,
    },
    buttonElementsCustGrid: {
        marginLeft: 6,
        marginRight: 6,
        width: deviceWidth / 3 - 18,
        height: 170,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#fff",
        elevation:0,
    },
    container: {
        backgroundColor: "#fff",
    },
    iosHead: {
        height: 22,
        backgroundColor: '#323232'
    },
    ColElements: {
        alignItems: 'center', 
        height: 170, 
        paddingBottom: 20, 
        paddingTop: 20, 
    },
    ColElementsCustGrid: {
        paddingLeft: 5,
        paddingRight: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:10,
    },
    imgColElements:{
        height:130,
        maxHeight: 135,
        width: (deviceWidth/3) - 10,
        borderRadius:10,
    },
    inputSearchColor: {
        color: "#323232",
        fontSize: 18,
        width:'90%',
    },
    imgSearchIconStyle: {
        height: 25,
    },
    RowElements: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        padding: 7,
        elevation:0,
    },
    searchBarView: {
        backgroundColor: "#f5f5f5",
        borderColor: '#ccc',
        borderRadius: 2,
        elevation: 2,
        margin:10,
        borderWidth:1,
        borderBottomWidth:0,
        borderTopWidth:3,
        borderBottomColor:'transparent',    
    },
    searchBarInput: {
        padding: 10,
    },
    searchWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
    },
    textColElements: {
        color: "#323232",
        fontSize: 15,
        fontWeight: '600',
        flex: 1,
        flexDirection:'row',
        paddingTop: 6, 
        width: "100%",
        textAlign: "center",
        flexWrap:'wrap',
    },
    textSearchStyle: {
        color: "#b08d58",
        fontSize: 18,     
        fontWeight: "600",
    },
};