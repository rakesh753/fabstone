
import React, { Component } from 'react';
import { Image, View, TouchableOpacity, AsyncStorage, ActivityIndicator, Text, Platform, Modal, Linking, Alert } from 'react-native';
import { Button, Container, Content, Icon, Input, Item } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import styles from "./homeScreenStyle";
import BannerHeader from "../../components/bannerHeader/index";
import Footer from "../../components/footer/index";
import * as URL from "../../constants/constants";
import axios from "axios";
import { connect } from 'react-redux';
import { settingIcon, contactIcon, searchIcon, specialIcon, aFrameIcon } from "../../actions/index";



String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowUpdateVersion: false,
      app_version: '',
      navigate: this.props.navigation.navigate,
      categoryVar: [],
      ElementInventoryloginDetail: 0,
      ElementInventoryloginDetailName: 0,
      isLoading: true,
      SettingsDefaultWarehouseAsFun: null,
      AvailableInventoryAs: null,
      nwMessage: ''
    }
  }

  redirectToCollectionProducts = (collectionName) => {
    this.props.searchIcon(0)
    this.state.navigate('CollectionProducts', {
      id: "123",
      title: collectionName,
      WarehouseSetting: this.state.SettingsDefaultWarehouseAsFun,
      AvailabilitySettings: this.state.AvailableInventoryAs,
    })
  }

  componentDidMount() {
    let my_app_version = '2.0'
    axios.get(`${URL.BASE_WP}current_app_version`)
      .then((res) => {
        if (my_app_version != res.data.play_store_version) {
          Alert.alert(
            'Update is Available',
            'An update for the application is available',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              { text: 'OK', onPress: () => Platform.OS === 'android' ? Linking.openURL("https://play.google.com/store/apps/details?id=com.elementsdesign") : Linking.openURL("https://apps.apple.com/us/app/elements-designs/id1411907346") },

            ],
            { cancelable: false },
          );
        }
      }).catch((err) => {
        console.log('current_app_version', err)
      })
    var that = this;
    axios
      .get(`${URL.BASE_WP}categories`)
      .then(function (response) {     
        if (response.data.status === 1) {
          that.setState({ categoryVar: response.data.result, isLoading: false, nwMessage: '' });
        } else {
          that.setState({
            isLoading: false,
            categoryVar: []
          })
        }
      })
      .catch(function (error) {
        that.setState({
          isLoading: false,
          categoryVar: []
        })
        console.log('categories', error);


        if (error.message === "Network Error") {
          Alert.alert('Opps...no connection')
          that.setState({
            nwMessage: "Please check your connection"
          })
        }

      });

    AsyncStorage.getItem("ViewInventoryAsSetting").then((value) => {
      value == null ?
        AsyncStorage.setItem('ViewInventoryAsSetting', 'photoView') : null
    }).done();

    AsyncStorage.getItem("SettingsDefaultWarehouse").then((value) => {
      value == null ?
        AsyncStorage.setItem('SettingsDefaultWarehouse', 'All') : null
    }).done();

    AsyncStorage.getItem("AvailableInventoryAsSetting").then((value) => {
      value == null ?
        AsyncStorage.setItem('AvailableInventoryAsSetting', 'PcOnHand') : null
    }).done();


    AsyncStorage.getItem('SettingsDefaultWarehouse').then((value) => {
      if (value === 'Fabricators Stone Group') {
        this.setState({ SettingsDefaultWarehouseAsFun: 'Fabricators Stone Group' })
      } else if (value === 'Fabstone Dallas') {
        this.setState({ SettingsDefaultWarehouseAsFun: 'Fabstone Dallas' })
      } else {
        this.setState({ SettingsDefaultWarehouseAsFun: 'All' })
      }
    }).done();

    AsyncStorage.getItem('AvailableInventoryAsSetting').then((value) => {
      if (value === 'PcOnHand') {
        this.setState({ AvailableInventoryAs: 'PcOnHand' })
      } else {
        this.setState({ AvailableInventoryAs: 'Available_QTY' })
      }
    }).done();
  }
  redirectToRefinedSearch() {
    this.state.navigate('RefinedSearch', { RefinedSearch: this.props })
    this.props.searchIcon(0)
  }

  render() {
    return (
      <Container style={styles.container} >
        {
          Platform.OS === 'ios' && (<View style={styles.iosHead} />)
        }
        <BannerHeader />

        <View searchBar style={styles.searchBarView} >
          <TouchableOpacity onPress={() => {
            this.props.searchIcon(0)
            this.state.navigate('SearchSimple')
          }}>
            <View style={styles.searchWrapper}>
              <Text style={styles.inputSearchColor} numberOfLines={1}>
                Search by product name, SKU
              </Text>
              <Image
                style={styles.imgSearchIconStyle}
                source={require('../../assets/input-search-icon.png')}
                resizeMode="contain"
              />
            </View>
          </TouchableOpacity>
        </View>

        <Content>
          {
            this.state.isLoading ?
              (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                  <ActivityIndicator size="large" color="#323232" />
                </View>

              ) :



              <Grid>
                <Row style={styles.RowElements}>
                  {
                    this.state.categoryVar.map((data, i) => {
                      let colTypeURL = data.CategoryName;
                      // let collName = data.CategoryName.capitalize();
                      // let ns = collName.includes("ELEMENTS") ? collName.split(" ")[1] : collName;
                      return (
                        <Button style={styles.buttonElementsCustGrid} key={i} onPress={() => this.redirectToCollectionProducts(colTypeURL)}>
                          <Col style={styles.ColElementsCustGrid} key={i}>
                            <Image
                              style={styles.imgColElements}
                              source={{ uri: data.ProductImage === null || data.ProductImage === "" ? URL.FABSTON_LOGO : data.ProductImage }}
                            // resizeMode="contain"
                            />
                            <Text numberOfLines={1} style={styles.textColElements} >{data.type}</Text>
                          </Col>
                        </Button>
                      )
                    })
                  }
                </Row>
              </Grid>
          }
          {this.state.nwMessage != "" ?
            < View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
              <Text style={styles.textSearchStyle}>{this.state.nwMessage}</Text>
            </View>
            : null}

        </Content>


        <View style={{ marginTop: 2, marginBottom: 2 }}>
          <Button block transparent onPress={() => this.redirectToRefinedSearch()}>
            <Text style={styles.textSearchStyle}>Refined Search</Text>
          </Button>
        </View>

        <Footer navigation={this.props.navigation} />
      </Container >
    );
  }
}
function mapStateToProps(state) {
  return {
    footerTabChangee: state.footerTabChange
  }
}

function mapDispatchToProps(dispatch) {
  return {
    settingIcon: (text) => dispatch(settingIcon(text)),
    contactIcon: (text) => dispatch(contactIcon(text)),
    searchIcon: (text) => dispatch(searchIcon(text)),
    specialIcon: (text) => dispatch(specialIcon(text)),
    aFrameIcon: (text) => dispatch(aFrameIcon(text)),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen)