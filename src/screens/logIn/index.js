

import React, { Component } from 'react';
import { View, Text, AsyncStorage, Linking, ActivityIndicator } from 'react-native';
import { Body, Button, Card, CheckBox, Container, Content, Form, Icon, Input, Item, ListItem } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import styles from "./logInScreenStyle";
import BannerHeader from "../../components/bannerHeader/index";
import Footer from "../../components/footer/index";
import * as URL from "../../constants/constants";
import axios from "axios";
import firebase, { RemoteMessage, Notification, NotificationOpen } from 'react-native-firebase';

export default class LogIn extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: null,
			error: 0,
			navigate: this.props.navigation.navigate,
			password: null,
			signInBox: 0,
			success: 0,
			ElementInventoryloginDetail: '0',
			ElementInventoryloginDetailName: '0',
			fcm: '',
			isLoading: false,
			messageError: ''
		}
	}

	static navigationOptions = {
		title: "Sign In",
	};


	redirectToHome = () => {
		var that = this;
		if (this.state.email != null && this.state.password != null) {
			that.setState({
				isLoading: true,
				messageError: ""
			})
			axios
				.post(`${URL.BASE_WP}login`, {
					email: this.state.email,
					password: this.state.password,
					fcm: this.state.fcm
				})
				.then(function (response) {
					// console.log(response.data)
					if (response.data.error) {
						alert(response.data.error);
						that.setState({
							isLoading: false
						})
					} else {
						that.setState({
							isLoading: false
						})
						AsyncStorage.setItem('ElementInventoryloginUser', JSON.stringify(response.data.user_data));
						AsyncStorage.setItem('ElementInventorylogin', "" + response.data.user_data.user_email);
						AsyncStorage.setItem('ElementInventoryloginID', "" + response.data.user_data.Id);
						AsyncStorage.setItem('ElementInventoryBusinessAddress', response.data.user_data.meta_data.business_address);
						AsyncStorage.setItem('ElementInventoryBusinessType', response.data.user_data.meta_data.business_type);
						AsyncStorage.setItem('ElementInventorycity', response.data.user_data.meta_data.city);
						AsyncStorage.setItem('ElementInventoryzip', response.data.user_data.meta_data.zip);
						AsyncStorage.setItem('ElementInventorycompany_name', response.data.user_data.meta_data.company_name);
						AsyncStorage.setItem('ElementInventorycompany_website', response.data.user_data.meta_data.company_website);
						AsyncStorage.setItem('ElementInventoryphone_number', response.data.user_data.meta_data.phone_number);
						AsyncStorage.setItem('ElementInventorystate_name', response.data.user_data.meta_data.state_name);
						const body = {
							"user_id": `${response.data.user_data.Id}`,
							"user_email": `${response.data.user_data.user_email}`,
							"fcm_key": `${that.state.fcm}`
						}
						axios.post(`${URL.BASE_WP}push_notification`, body, {
						})
							.then((res) => {
							}).catch((err) => {
								console.log('pushnotification api err', err)
							})
						that.setState({ success: 1, })
					}
				})
				.catch(function (error) {
					console.log(error);
					that.setState({
						isLoading: false
					})
				});
		} else {
			that.setState({
				messageError: "Please enter email and password"
			})
		}

	}

	componentDidMount = async () => {
		/**
		 * Monitor token generation
		 */
		this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(async fcmToken => {
			// Process your token as required
			await this._getToken();
		});

		await this._checkPermission();
		this._onMessageReceived();
		this._onNotificationDisplay();
		this._onNotificationOpen();
	}

	componentWillUnmount = () => {
		this.onTokenRefreshListener();
		this.messageListener();
		this.notificationDisplayedListener();
		this.notificationListener();
		this.notificationOpenedListener();
	}

	/**
   * Check permissions
   */
	_checkPermission = async () => {
		const enabled = await firebase.messaging().hasPermission();
		if (enabled) {
			await this._getToken();
		} else {
			this._requestPermission();
		}
	}

	/**
	 * Retrieve the current registration token
	 */
	_getToken = async () => {
		const fcmToken = await firebase.messaging().getToken();

		if (fcmToken) {
			console.log('fcmToken',fcmToken)
			this.setState({
				fcm: fcmToken
			})
		} else {
		}
	}

	/**
	 * Request permissions
	 */
	_requestPermission = async () => {
		console.log('In request permission');
		try {
			await firebase.messaging().requestPermission();
			// User has authorised
			this._getToken();
		} catch (error) {
			// User has rejected permissions
			console.log('permission rejected');
		}
	}

	/**
	 * Listen for FCM messages
	 */
	_onMessageReceived = () => {

		/**
		 * A message will trigger the onMessage listener when the application receives a message in the foreground.
		 */
		this.messageListener = firebase.messaging().onMessage((message: RemoteMessage) => {
			// Process your message as required
			console.log('RemoteMessage', message);
		});
	}

	/**
	 * Listeners of Notification
	 */
	_onNotificationDisplay = () => {

		/** Triggered when a particular notification has been displayed */
		this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification: Notification) => {
			// Process your notification as required
			// ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
			console.log('onNotificationDisplayed', notification);
		});

		/** Triggered when a particular notification has been received */
		this.notificationListener = firebase.notifications().onNotification((notification: Notification) => {
			// Process your notification as required
			console.log('onNotification', notification);
		});
	}

	/**
	 * Listen for a Notification being opened
	 */
	_onNotificationOpen = async () => {

		/** App in Foreground and background */
		this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
			// Get the action triggered by the notification being opened
			const action = notificationOpen.action;
			// Get information about the notification that was opened
			const notification: Notification = notificationOpen.notification;
			console.log('FG action', action);
			console.log('FG notification', notification);
		});

		/** App Closed */
		const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
		if (notificationOpen) {
			// App was opened by a notification
			// Get the action triggered by the notification being opened
			const action = notificationOpen.action;
			// Get information about the notification that was opened
			const notification: Notification = notificationOpen.notification;
			// console.log('Closed action', action);
			// console.log('Closed notification', notification);
		}
	}


	whatDoINeedToSignIn() {
		this.props.navigation.navigate('WhatDoINeedToSignIn');
	}
	forgotPassword() {
		this.props.navigation.navigate('ResetPswd');
	}
	RequestAccess() {
		this.props.navigation.navigate('RequestAccess');
	}
	needHelp() {
		Linking.openURL('https://elements.design/contact/')
	}

	render() {

		this.state.success == 1 ? (this.props.navigation.goBack(), this.state.navigate('HomeScreen', { Home: "HomeScreen" })) : null
		return (
			<Container>
				<Content>
					<View style={styles.container}>
						<BannerHeader />
						<Form style={styles.formContainer}>
							<Item style={styles.formItem}>
								<Input
									placeholder="Email"
									onChangeText={(email) => { this.setState({ email }); }}
									value={this.state.email}
									style={styles.formInput}
									keyboardType={'email-address'}
									autoCapitalize="none"
									returnKeyType={'done'}

								/>
								{this.state.error == 1 ? <Icon style={styles.error} name='close-circle' /> : null}
							</Item>
							<Item style={styles.formItem}>
								<Input
									secureTextEntry={true}
									placeholder="Password"
									onChangeText={(password) => { this.setState({ password }); }}
									value={this.state.password}
									style={styles.formInput}
									returnKeyType={'done'}
								/>
								{this.state.error == 1 ? <Icon style={styles.error} name='close-circle' /> : null}
							</Item>
							<Text style={{ color: 'red', marginLeft: 15 }}>{this.state.messageError}</Text>

							<Button style={styles.formButtonSub} onPress={() => this.redirectToHome()}>

								{this.state.isLoading ?
									<ActivityIndicator size="large" color="#fff" style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }} />
									:
									<Text style={styles.formButtonSubText}>Sign In</Text>}
							</Button>

							<ListItem style={styles.listItemCheckBox}>
								<CheckBox
									checked={this.state.signInBox === 1 ? true : false}
									color="#323232"
									onPress={() => this.state.signInBox === 0 ? this.setState({ signInBox: 1 }) : this.setState({ signInBox: 0 })}
								/>
								<Body>
									<Text style={styles.listItemCheckBoxText}>Keep me signed in</Text>
								</Body>
							</ListItem>

						</Form>
					</View>
					<Grid style={{ backgroundColor: "#fff", marginRight: 25, marginBottom: 15 }}>
						<Row size={20}>
							<Col>
								<Button block transparent onPress={() => this.RequestAccess()}>
									<Text style={styles.helpTextLeft}> Request Access </Text>
								</Button>
							</Col>
							<Col>
								<Button block transparent style={styles.helpButtonRight} onPress={() => this.needHelp()}>
									<Text style={styles.helpTextRight}> Need Help? </Text>
								</Button>
							</Col>
						</Row>
						<Row size={20} style={styles.helpRowCenter}>
							<Button block transparent onPress={() => this.whatDoINeedToSignIn()}>
								<Text style={styles.helpTextCenter}> Why do I need to sign in?</Text>
							</Button>
						</Row>
						<Row size={15} style={styles.helpRowCenterForgetPass}>
							<Button block transparent onPress={() => this.forgotPassword()}>
								<Text style={styles.helptextCenterForgetPass}> Forgot Password? </Text>
							</Button>
						</Row>
					</Grid>
				</Content>

				<Footer navigation={this.props.navigation} />
			</Container>
		);
	}
}