

import React, { Component } from "react";
import { AsyncStorage, Platform, Text, View, Image, BackHandler, TouchableOpacity } from "react-native";
import { Input, Container, Content, Button, Icon, Picker, Form, Item as FormItem } from "native-base";
import styles from './refinedSearchStyle';
import Footer from "../../components/footer/index";
import * as URL from "../../constants/constants";
import axios from "axios";
import { connect } from 'react-redux';
import { settingIcon, contactIcon, searchIcon, specialIcon, aFrameIcon } from "../../actions/index";
const Item = Picker.Item;

class PickerExample extends Component {

	static navigationOptions = ({ navigation }) => ({
		title: 'Refined Search',
		headerLeft: <Icon
			name='arrow-back'
			onPress={() => {
				const { params } = navigation.state;
				const RefinedSearchA = params ? params.RefinedSearch : null;
				RefinedSearchA.searchIcon(1)
				navigation.goBack()
			}}
			style={{ color: '#fff', marginLeft: 10 }}
		/>,
	});

	constructor(props) {
		super(props);
		this.state = {
			AvailableInventory: "all",
			warehouse: "all",
			color: "all",
			MaterialSeries: "all",
			MaterialType: "all",
			PriceGroup: "all",
			MaterialThickness: "all",
			navigate: this.props.navigation.navigate,
			categoryVar: [],
			ElementInventoryloginDetailCollectionProduct: 0,

			allInventoryColor: [],
			allInventoryLocation: [],
			ProductGroupNew: [],
			userRole: 'All'		
		};
	}

	componentWillMount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }
	componentWillUnmount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }

	handleBackButtonClick = () => {
		this.props.settingIcon(0)
		this.props.contactIcon(0)
		this.props.searchIcon(0)
		this.props.specialIcon(0)
		this.props.aFrameIcon(0)
	}
	searchRefined = () => {
		AsyncStorage.setItem('refine_search_warehouse', this.state.warehouse);
		AsyncStorage.setItem('refine_search_Available', this.state.AvailableInventory);
		(this.state.AvailableInventory !== 'all' || this.state.warehouse !== '' || this.state.color !== 'all' || this.state.MaterialSeries !== 'all' || this.state.MaterialType !== 'all' || this.state.PriceGroup !== 'all' || this.state.MaterialThickness !== 'all') ?
			this.state.navigate('RefinedSearchResult', { AvailableInventoryT: this.state.AvailableInventory, warehouse: this.state.warehouse, color: this.state.color, MaterialSeries: this.state.MaterialSeries, MaterialType: this.state.MaterialType, PriceGroup: this.state.PriceGroup, MaterialThickness: this.state.MaterialThickness,userRole:this.state.userRole })
			:
			alert('Please Select your choice first')
	}
	_getColorOnload = () => {
		axios.get(`${URL.BASE_WP}refine_search_params`)
			.then((res) => {
				this.setState({
					allInventoryColor: [],
					ProductGroupNew: [],
					categoryVar: [],
					allInventoryLocation: []
				})
				if (res.data.material_color.status === 1) {
					this.setState({
						allInventoryColor: res.data.material_color.result
					})
				}
				if (res.data.material_series.status === 1) {
					this.setState({
						ProductGroupNew: res.data.material_series.result
					})
				}
				if (res.data.material_type.status === 1) {
					this.setState({
						categoryVar: res.data.material_type.result
					})
				}
				if (res.data.warehouse.status === 1) {
					this.setState({
						allInventoryLocation: res.data.warehouse.result
					})
				}
			})
	}



	componentDidMount() {
		this._getColorOnload();

		AsyncStorage.getItem("ElementInventorylogin").then((value) => {
			value !== null ?
				this.setState({ "ElementInventoryloginDetailCollectionProduct": value })
				: this.setState({ "ElementInventoryloginDetailCollectionProduct": '0' })
		}).done();

		AsyncStorage.getItem("ElementInventoryBusinessType").then((value) => {
			value !== null ?
			this.setState({ "userRole": value })
				: this.setState({ "userRole": 'All' })

		}).done();


	}

	onValueChange1 = (value) => {
		this.setState({
			AvailableInventory: value
		});
	}
	onValueChange2 = (value) => {
		this.setState({
			warehouse: value
		});
	}
	onValueChange3 = (value) => {
		this.setState({
			color: value
		});
	}
	onValueChange4 = (value) => {
		this.setState({
			MaterialSeries: value
		});
	}
	onValueChange5 = (value) => {
		this.setState({
			MaterialType: value
		});
	}
	onValueChange6 = (value) => {
		this.setState({
			PriceGroup: value
		});
	}
	onValueChange7 = (value) => {
		this.setState({
			MaterialThickness: value
		});
	}

	render() {
		return (
			<Container style={styles.container} >
				<Content>
					<Form>
						<View style={styles.mainView}>
							<View style={styles.innerMainView1} >
								<Picker
									mode="dropdown"
									selectedValue={this.state.AvailableInventory}
									onValueChange={(value) => this.onValueChange1(value)}
									style={styles.picker1}
								>
									<Item style={styles.picker1Item} label="Available Inventory" value="all" />
									<Item style={styles.picker1Item} label="Available" value="Available_QTY" />
									<Item style={styles.picker1Item} label="On Hand" value="PcOnHand" />
								</Picker>
							</View>
							<View style={styles.innerMainView2} >
								<Picker
									mode="dropdown"
									selectedValue={this.state.warehouse}
									onValueChange={(value) => this.onValueChange2(value)}
									style={styles.picker2}
								>
									<Item label="Warehouses" value="all" />

									{
										this.state.allInventoryLocation.map((data, i) => {
											return <Item label={data.Parentloc_PrintName} value={data.Parentloc_PrintName} key={i} />
										})
									}

								</Picker>
							</View>
						</View>
						<View style={styles.subMainView}>
							<Picker
								mode="dropdown"
								selectedValue={this.state.color}
								onValueChange={(value) => this.onValueChange3(value)}
								style={styles.picker3}
							>
								<Item label="Material Color" value="all" />

								{
									this.state.allInventoryColor.map((data, i) => {
										return <Item label={data.Item_BaseColor} value={data.Item_BaseColor} key={i} />
									})
								}
							</Picker>
						</View>
						<View style={styles.subMainView}>
							<Picker
								mode="dropdown"
								selectedValue={this.state.MaterialSeries}
								onValueChange={(value) => this.onValueChange4(value)}
								style={styles.picker3}
							>
								<Item label="Material Series" value="all" />
								{this.state.ProductGroupNew.map((data, i) => {
									return <Item label={data.ProductGroup} value={data.ProductGroup} key={i} />
								})}
							</Picker>
						</View>
						<View style={styles.subMainView}>
							<Picker
								mode="dropdown"
								selectedValue={this.state.MaterialType}
								onValueChange={(value) => this.onValueChange5(value)}
								style={styles.picker3}
							>
								<Item label="Material Type" value="all" />
								{
									this.state.categoryVar.map((data, i) => {
										return <Item label={data.CategoryName} value={data.CategoryName} key={i} />
									})
								}
							</Picker>
						</View>
						<View style={styles.subMainView}>
							{this.state.ElementInventoryloginDetailCollectionProduct !== '0' ? (
								<Picker
									mode="dropdown"
									selectedValue={this.state.PriceGroup}
									onValueChange={(value) => this.onValueChange6(value)}
									style={styles.picker3}
								>
									<Item label="Price Group" value="all" />
									<Item label="$0-$10" value="$0-$10" />
									<Item label="$10-$20" value="$10-$20" />
									<Item label="$20-$30" value="$20-$30" />
									<Item label="$30+" value="$30-$999" />
								</Picker>
							) : (
									<Picker
										mode="dropdown"
										selectedValue={this.state.PriceGroup}
										onValueChange={(value) => this.onValueChange6(value)}
										style={styles.picker3}
									>
										<Item label="Price Group" value="all" />
										<Item label="$" value="$0-$10" />
										<Item label="$$" value="$10-$20" />
										<Item label="$$$" value="$20-$30" />
										<Item label="$$$$" value="$30-$999" />
									</Picker>
								)}
						</View>
						<View style={styles.subMainView}>
							<Picker
								mode="dropdown"
								selectedValue={this.state.MaterialThickness}
								onValueChange={(value) => this.onValueChange7(value)}
								style={styles.picker3}
							>
								<Item label="Material Thickness" value="all" />
								<Item label="2 cm" value="2cm" />
								<Item label="3 cm" value="3cm" />

							</Picker>
						</View>
						<View style={styles.subSearchMainView}>
							<View style={styles.searchButtonView}>
								<Button style={styles.searchButton} onPress={() => this.searchRefined()} transparent><Text style={styles.searchButtonText}>Search</Text></Button>
							</View>
						</View>
					</Form>
				</Content>
				<Footer navigation={this.props.navigation} />
			</Container>
		);
	}
}
function mapStateToProps(state) {
	return {
		footerTabChangee: state.footerTabChange
	}
}
function mapDispatchToProps(dispatch) {
	return {
		settingIcon: (text) => dispatch(settingIcon(text)),
		contactIcon: (text) => dispatch(contactIcon(text)),
		searchIcon: (text) => dispatch(searchIcon(text)),
		specialIcon: (text) => dispatch(specialIcon(text)),
		aFrameIcon: (text) => dispatch(aFrameIcon(text)),
	}
}
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(PickerExample)