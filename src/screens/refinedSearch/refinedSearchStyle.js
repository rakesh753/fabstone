const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceWidth = Dimensions.get("window").width;

export default {
	container: {
		backgroundColor: "#fff",
	},
	mainView: {
		flex: 1,
		flexDirection: "row",
	},
	innerMainView1: {
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
		backgroundColor: "#CCCCCC",
		margin: 10,
		marginTop: 20,
		marginRight: 5,
	},
	innerMainView2: {
		alignItems: 'flex-end',
		justifyContent: 'flex-end',
		backgroundColor: "#CCCCCC",
		margin: 10,
		marginTop: 20,
		marginRight: 5,
		overflow: 'hidden'
	},
	picker1: {
		width: deviceWidth / 2 - 15,

	},
	picker2: {
		width: deviceWidth / 2 - 20,
	},
	picker3: {
		width: deviceWidth - 20,
	},
	picker1Item: {
		color: "#fff",
	},
	searchButtonView: {
		alignItems: 'center',
		justifyContent: 'center',
	},
	searchButton: {
		backgroundColor: "#CCCCCC",
		alignItems: 'center',
		justifyContent: 'center',
		width: deviceWidth / 2,
		margin: 10,
	},
	searchButtonText: {
		color: "#000",
	},
	subSearchMainView: {
		alignItems: 'center',
		justifyContent: 'center',
	},
	subMainView: {
		backgroundColor: "#CCCCCC",
		margin: 10,
	},
	formInput: {
		backgroundColor: "#f5f5f5",
		borderColor: '#ccc',
		borderWidth: 1,
		borderRadius: 5,
		paddingLeft:10
	},
	imgSearchIconStyle: {
		height: 20,
	},

};