import React, { Component } from 'react';
import { Text, Image, View, ActivityIndicator, TouchableOpacity } from 'react-native';
import { Container, Content, } from 'native-base';
import styles from './style';
import Footer from "../../components/footer/index";
import * as URL from "../../constants/constants";
import axios from "axios";

export default class Settings extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: "Refined Search Result",
    });

    constructor(props) {
        super(props);
        this.state = {
            RefinedSearchData: [],
            navigate: this.props.navigation.navigate,
            isLoading: true,
            data_name: null
        };
    }
    componentDidMount() {
        var that = this
        const { state } = this.props.navigation;
        let AvailableInventory = state.params ? state.params.AvailableInventoryT : "";
        let warehouse = state.params ? state.params.warehouse : "";
        let color = state.params ? state.params.color : "";
        let MaterialSeries = state.params ? state.params.MaterialSeries : "";
        let MaterialType = state.params ? state.params.MaterialType : "";
        let PriceGroup = state.params ? state.params.PriceGroup : "";
        let MaterialThickness = state.params ? state.params.MaterialThickness : "";
        let userRole = state.params ? state.params.userRole : "";


        let finalData = {
            "available": `${AvailableInventory === "all" ? "All" : AvailableInventory}`,
            "warehouse": `${warehouse === "all" ? "All" : warehouse}`,
            "product_group": `${MaterialSeries === "all" ? "All" : MaterialSeries}`,
            "category": `${MaterialType === "all" ? "All" : MaterialType}`,
            "color": `${color === "all" ? "All" : color}`,
            "material_thickness": `${MaterialThickness === "all" ? "All" : MaterialThickness}`,
            "price_group": `${PriceGroup === "all" ? "All" : PriceGroup}`,
            "user_role": `${userRole}`,
        }   
        axios.post(`${URL.REFINED_SEARCH}`, finalData, {})
            .then(function (response) {      
                if (response.data.status === 1) {
                    that.setState({ success: 1, RefinedSearchData: response.data.result, isLoading: false })
                } else {
                    that.setState({ RefinedSearchData: [], isLoading: false })
                }
            })
            .catch(function (error) {
                that.setState({ RefinedSearchData: [], isLoading: false })
            });
    }
    GetListViewItem(data) {
        this.state.navigate("SingleProductSlider", { ProData: JSON.stringify(data), ProDataSKU: data.SKU, "refind_search": "refind_search" })
    }

    render() {
        return (
            <Container style={styles.container} >
                <Content>
                    <View>
                        {this.state.isLoading ?
                            (<View style={{ flex: 1, justifyContent: 'center', alignContent: 'center' }}>
                                <ActivityIndicator size="large" color="#323232" />
                            </View>) : (
                                <View >
                                    {
                                        this.state.RefinedSearchData.length > 0 ? (
                                            this.state.RefinedSearchData.map((data, i) => {
                                                return (
                                                    <View key={i}>
                                                        {data.ProductName == null ? null :
                                                            <TouchableOpacity onPress={() => this.GetListViewItem(data)}>
                                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 10, width: "100%" }}>
                                                                    <View style={{ width: "92%", }}>
                                                                        <Text numberOfLines={1} style={styles.rowViewContainer}>{data.ProductName === null ? this.state.data_name : data.ProductName.substr(0, 40)} </Text>
                                                                    </View>
                                                                    <View style={{ width: "8%", alignItems: 'center' }}>
                                                                        <Image
                                                                            style={styles.imgSearchIconStyle}
                                                                            source={require('../../assets/arrow.png')}
                                                                            resizeMode="contain"
                                                                        />
                                                                    </View>
                                                                </View>
                                                            </TouchableOpacity>
                                                        }
                                                    </View>
                                                )
                                            })
                                        ) : <Text style={{ fontSize: 20, padding: 20 }}>No Product Available</Text>
                                    }
                                </View>)
                        }
                    </View>
                </Content>
                <Footer navigation={this.props.navigation} />
            </Container>
        )
    }
}