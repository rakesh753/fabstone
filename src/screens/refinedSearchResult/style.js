const React = require("react-native");
const { Dimensions, Platform } = React;
import color from "color";

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

export default {
    container: {
        backgroundColor: "#fff",
    },
    rowViewContainer: {
        fontSize: 17,
    },
    imgSearchIconStyle: {
        height: 20,
    },
}