import React, { Component } from 'react';
import { Text, Image, View, ActivityIndicator, AsyncStorage, ScrollView, TouchableOpacity } from 'react-native';
import { Input, Item, } from 'native-base';
import styles from './style';
import * as URL from "../../constants/constants";
import axios from "axios";
import { connect } from 'react-redux';
import { updateText } from "../../actions/index";



function mapStateToProps(state) {
    return {
        footerTabChangee: state.footerTabChange
    }
}

function mapDispatchToProps(dispatch) {
    return {
        updateText: (text) => dispatch(updateText(text)),
    }
}
class SearchSimple extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: "Inventory Search",
    });

    constructor(props) {
        super(props);
        this.state = {
            navigate: this.props.navigation.navigate,
            isLoading: false,
            text: '',
            recentSearchAsyncData: [],
            searchText: null,
            dataSource: [],
            SettingsDefaultWarehouseAsFun: null,      
            typeData: false,
        }
        this.arrayholder = [];
    }

    componentDidMount() {
        AsyncStorage.getItem("recentSeactAsync").then((value) => {
            if (value !== null) {
                this.setState({ "recentSearchAsyncData": JSON.parse(value) });
            }
        }).done();

        AsyncStorage.getItem("SettingsDefaultWarehouse").then((value) => {
            if (value !== null) {
                this.setState({ "SettingsDefaultWarehouseAsFun": value });
            }
        }).done();    
    }

    GetListViewItem(data) {
        if ((this.state.recentSearchAsyncData !== null) && (this.state.recentSearchAsyncData !== '') && (this.state.recentSearchAsyncData.length > 0)) {
            let arrdemo = new Array()
            let convParse = this.state.recentSearchAsyncData
            convParse.map((asyncData, i) => {
                if (asyncData.sku !== data.SKU) {
                    let pushdemo = arrdemo.push(asyncData)
                }
            })
            let convStr = data
            let pushdemo1 = arrdemo.push(data)
            AsyncStorage.setItem('recentSeactAsync', JSON.stringify(arrdemo));
        } else {
            let arrdemo = new Array()
            let convStr = data
            let pushdemo1 = arrdemo.push(data)
            AsyncStorage.setItem('recentSeactAsync', JSON.stringify(arrdemo));
        }
        this.state.navigate("SingleProductSlider", { ProData: JSON.stringify(data), ProDataSKU: data.SKU });
    }
    GetListViewItemRecentSearched(data) {
        this.state.navigate("SingleProductSlider", { ProData: JSON.stringify(data), ProDataSKU: data.SKU });
    }

    SearchFilterFunction(text) {
        const { SettingsDefaultWarehouseAsFun } = this.state;

        if (text.length.toString() > 3) {
            this.props.updateText(text)
            var that = this
            that.setState({ isLoading: true })
            let finalData = {    
                "warehouse": SettingsDefaultWarehouseAsFun,
                "name": text
            }
            axios
                .post(`https://elements.design/wp-json/elements_routes/normal_search`, finalData, {})
                .then(function (response) {                 
                    if (response.data.status === 1) {
                        that.setState({ dataSource: response.data.result, isLoading: false });
                    } else {
                        that.setState({ dataSource: [], isLoading: false, typeData: true });
                    }
                })
                .catch(function (error) {
                    that.setState({ dataSource: [], isLoading: false });
                    console.log('REFINED_SEARCH', error);
                });

        } else {
            this.props.updateText(null)
            this.setState({ dataSource: [], isLoading: false });
        }
    }
    searchBarRemoveValue = () => {
        let that = this
        this.props.updateText(null)
        that.setState({ dataSource: [], isLoading: false });
    }


    ListViewItemSeparator = () => {
        return (
            <View
                style={{
                    width: "100%",
                }}
            />
        );
    }

    render() {
        const { updateText } = this.props.footerTabChangee
        return (
            <View style={styles.simpleSearchWrapper}>
                <Item>
                    <Input placeholder="Search by product name, SKU" style={styles.inputSearchColor}
                        onChangeText={(text) => { this.SearchFilterFunction(text) }}
                        value={updateText}
                        autoFocus={true}
                    />
                    <TouchableOpacity onPress={() => this.searchBarRemoveValue()}>
                        <Image
                            style={styles.imgSearchIconStyle}
                            source={require('../../assets/cancle_cross.png')}
                            resizeMode="contain"
                        />
                    </TouchableOpacity>
                </Item>
                {this.state.isLoading ?
                    (<View style={{ flex: 1, paddingTop: 20 }}>
                        <ActivityIndicator size="large" color="#323232" />
                    </View>)
                    :
                    (<View style={styles.wrapperSearch}>
                        <View style={styles.searchBarView}>
                            <ScrollView>
                                {
                                    this.state.dataSource.length > 0 ? (
                                        this.state.dataSource.map((data, i) => {
                                            return (
                                                <View key={i}>
                                                    <TouchableOpacity onPress={() => this.GetListViewItem(data)}>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: "100%", padding: 10, alignSelf: 'center' }}>
                                                            <View style={{ width: "92%", }}><Text style={styles.rowViewContainer}>{data.ProductName} </Text></View>
                                                            <View style={{ width: "8%", }}>
                                                                <Image
                                                                    style={styles.imgSearchIconStyle}
                                                                    source={require('../../assets/arrow.png')}
                                                                    resizeMode="contain"
                                                                />
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            )
                                        })
                                    ) : (
                                            this.state.typeData ?
                                                (<View><Text style={{ fontSize: 20, padding: 10, color: '#000', textAlign: 'center' }}>No Data Found</Text></View>) : null
                                        )
                                }
                            </ScrollView>
                        </View>

                        <View style={styles.recentSearchBarView}>
                            {
                                this.state.recentSearchAsyncData.length > 0 ? (
                                    <Text style={styles.recentSearchHeading}>Recent Searches</Text>
                                ) : null
                            }
                            <ScrollView>

                                {
                                    this.state.recentSearchAsyncData.length > 0 ? (
                                        this.state.recentSearchAsyncData.map((data, i) => {
                                            return (
                                                <View key={i}>
                                                    <TouchableOpacity onPress={() => this.GetListViewItemRecentSearched(data)}>
                                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: "100%", padding: 10 }}>
                                                            <View style={{ width: "92%" }}><Text style={styles.rowViewContainer}>{data.ProductName} </Text></View>
                                                            <View style={{ width: "8%" }}>
                                                                <Image
                                                                    style={styles.imgSearchIconStyle}
                                                                    source={require('../../assets/arrow.png')}
                                                                    resizeMode="contain"
                                                                />
                                                            </View>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>)
                                        })
                                    ) : null
                                }

                            </ScrollView>
                        </View>
                    </View>)
                }
            </View>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SearchSimple)