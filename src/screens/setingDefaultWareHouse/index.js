import React, { Component } from 'react';
import { Text, View, AsyncStorage, TouchableOpacity } from 'react-native';
import { Container, Content, Icon, } from 'native-base';
import styles from './style';

export default class SettingsDefaultWarehouse extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: "Warehouse",
    });
    constructor(props) {
        super(props);
        this.state = {
            SettingsDefaultWarehouseAsFun: '0',
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('SettingsDefaultWarehouse').then((value) => {
            value !== null ?
                this.setState({ SettingsDefaultWarehouseAsFun: value })
                :
                this.setState({ SettingsDefaultWarehouseAsFun: '0' })
        }).done();
    }

    DefaultWarehousef = (data) => {
        AsyncStorage.setItem('SettingsDefaultWarehouse', data)
        this.setState({ SettingsDefaultWarehouseAsFun: data }, () => {
            this.props.navigation.state.params.refreshValues();
        })
        this.props.navigation.goBack()
    }
    render() {
        return (
            <Container style={styles.container} >
                <Content>
                    <View style={styles.parentWrapper}>
                        <TouchableOpacity onPress={() => this.DefaultWarehousef('Fabricators Stone Group')}>
                            <View style={styles.viewInner}>
                                <Text value="Fabricators Stone Group" style={styles.innerText}>Fabstone McKinney</Text>
                                {this.state.SettingsDefaultWarehouseAsFun == 'Fabricators Stone Group' ?
                                    <Icon name="md-checkmark-circle" size={10} style={styles.imgIconStyle} />
                                    : null}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.DefaultWarehousef('Fabstone Dallas')}>
                            <View style={styles.viewInner}>
                                <Text style={styles.innerText} value="Fabstone Dallas" >Elements Dallas</Text>
                                {this.state.SettingsDefaultWarehouseAsFun == 'Fabstone Dallas' ?
                                    <Icon name="md-checkmark-circle" size={10} style={styles.imgIconStyle} />
                                    : null}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.DefaultWarehousef('All')}>
                            <View style={styles.viewInner}>
                                <Text style={styles.innerText} value="All" >All Locations</Text>
                                {this.state.SettingsDefaultWarehouseAsFun == 'All' ?
                                    <Icon name="md-checkmark-circle" size={10} style={styles.imgIconStyle} />
                                    : null}
                            </View>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        )
    }
}