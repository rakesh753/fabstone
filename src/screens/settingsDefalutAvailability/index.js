import React, { Component } from 'react';
import { Text, View, AsyncStorage, TouchableOpacity } from 'react-native';
import { Container, Content, Icon, } from 'native-base';
import styles from './style';


export default class SettingsDefaultAvailability extends Component {

    static navigationOptions = ({ navigation }) => ({
        title: "Availability",
    });

    constructor(props) {
        super(props);
        this.state = {
            AvailableInventoryAs: '0',
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('AvailableInventoryAsSetting').then((value) => {
            value !== null ?
                this.setState({ AvailableInventoryAs: value })
                :
                this.setState({ AvailableInventoryAs: '0' })
        }).done();
    }
    AvailableInventoryView = (data) => {
        AsyncStorage.setItem('AvailableInventoryAsSetting', data)
        this.setState({ AvailableInventoryAs: data }, () => {
            this.props.navigation.state.params.refreshValues();
        })
        this.props.navigation.goBack()
    }
    render() {
        return (
            <Container style={styles.container} >
                <Content>

                    <View style={styles.parentWrapper}>
                        <TouchableOpacity onPress={() => this.AvailableInventoryView('Available_QTY')}>
                            <View style={styles.viewInner}>
                                <Text value="Available_QTY" style={styles.innerText}>Available</Text>
                                {this.state.AvailableInventoryAs == 'Available_QTY' ?
                                    <Icon name="md-checkmark-circle" size={10} style={styles.imgIconStyle} />
                                    : null}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.AvailableInventoryView('PcOnHand')}>
                            <View style={styles.viewInner}>
                                <Text style={styles.innerText} value="PcOnHand" >on-Hand</Text>
                                {this.state.AvailableInventoryAs == 'PcOnHand' ?
                                    <Icon name="md-checkmark-circle" size={10} style={styles.imgIconStyle} />
                                    : null}
                            </View>
                        </TouchableOpacity>
                    </View>
                </Content>
            </Container>
        )
    }
}