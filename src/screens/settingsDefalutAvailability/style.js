const React = require("react-native");
const { Dimensions, Platform } = React;

const deviceWidth = Dimensions.get("window").width;

export default {
    container: {
		backgroundColor: "#fff",
	},
	parentWrapper:{
		padding:10,
	},
	imgIconStyle:{
		position:'absolute',
		right:5,
		top:2,
		color:'#b08d58',
	},
	viewInner:{
		flexDirection:'row',
		justifyContent:'space-between',
		backgroundColor:'#fff',
		margin:5,
		padding:7,
		borderColor:'#dcdcdc',
		borderBottomWidth:1,
	},
	innerText:{
		position:'relative',
		fontSize:15,
		width:deviceWidth - 30,
		color:'#000',
		paddingBottom:7
	}
}