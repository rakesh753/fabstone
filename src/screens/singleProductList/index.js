import React, { Component } from 'react';
import { Image, Text, View, Share, AsyncStorage, BackHandler, ActivityIndicator, TouchableOpacity, Platform, Linking, Dimensions } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Button, Card, CardItem, H3, Icon, Container, Content, List, ListItem } from 'native-base';
import styles from "./singleProductListStyle";
import Footer from "../../components/footer/index";
import * as URL from "../../constants/constants";
import axios from "axios";
import { connect } from 'react-redux';
import { settingIcon, contactIcon, searchIcon, specialIcon, aFrameIcon } from "../../actions/index";


String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.toProperCase = function () {
  return this.toLowerCase().replace(/^(.)|\s(.)/g,
    function ($1) { return $1.toUpperCase(); });
}

const deviceWidth = Dimensions.get("window").width;
const adjustsFontSizeToFit = (deviceWidth >= 375 ? false : true);

class CollectionProducts extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: "Material Details",
    headerLeft: <Icon
      name='arrow-back'
      onPress={() => navigation.state.params.increaseCount()}
      style={{ color: '#fff', marginLeft: 10 }}
    />
  });

  precisionRound = (number) => {
    let no = number.replace("$", "");
    return `$${+(Math.round(no * 100) / 100)}`;
  }

  precisionRoundWithoutPoint = (number) => {
    let no = number.replace("$", "");
    return `$${+(Math.round(Math.trunc(no) * 100) / 100)}`;
  }

  _checkUserCatPrice = (item) => {
    const { userLoginPrice } = this.state;
    let switchKey = userLoginPrice.toLowerCase();

    switch (switchKey) {
      case "contractor":
        return this.precisionRound(item.ConPrice);
        break;

      case "designer":
        return this.precisionRound(item.DesPrice);
        break;

      case "fabricator":
        return this.precisionRound(item.FabPrice);
        break;

      case "Fabricatorb":
        return this.precisionRound(item.FabPrice);
        break;

      case "retail":
        return this.precisionRound(item.RetPrice);
        break;

      default:
        return this.precisionRound(item.UnitCost);
        break;
    }
  }

  optionsWithSlider = () => {
    this.setState({ sliderViewOptions: true, })
  }

  showSliderView = () => {
    const { state } = this.props.navigation;
    let ProData = state.params.ProData ? JSON.parse(state.params.ProData) : "<undefined>";
    let proSKU = state.params.ProDataSKU ? state.params.ProDataSKU : "<undefined>";
    this.state.InventoryViewSetting == 'photoView' ?
      this.props.navigation.goBack()
      :
      this.props.navigation.goBack()
    this.props.navigation.navigate('SingleProductSlider', { ProData: JSON.stringify(ProData), ProDataSKU: proSKU })
    this.setState({
      sliderViewOptions: false,
    })
  }

  enlargeImage = (img) => {
    this.setState({
      sliderViewOptions: false,
    })
    this.state.navigate('EnlargeImage', { img: img })
  }

  shareProductSlide = (data) => {
    let msgString = "Name: " + data.main.ProductName.toUpperCase() + "\n" + "Type: " + data.main.Category + "\n" + "Series: " + data.main.ProductGroup + "\n" + "SKU: " + data.main.SKU + "\n\n";

    data.bundles !== "Record not found!" && data.bundles.map((val, index) => {
      msgString += "Bundle Number: " + val.Bundle + "\nBlock Number: " + val.Block + "\nAvg Size: " + val.Dimensions + "\" \n" + "On Hand: " + val.PCOnHand + "\nAvailable: " + val.CurrentAvailabeQty + "\nLocation: " + this.locShowUser(val.LocationName) + "\n\n";
    });

    let ImgUrl = ''
    if (data.main.item_Filename !== "" && data.main.item_Filename !== null) {
      ImgUrl = URL.BaseImgUrl + data.main.item_Filename;
    } else {
      ImgUrl = URL.FABSTON_LOGO;
    }

    msgString += ImgUrl;

    Share.share(
      {
        message: msgString, 
      }).then(result => console.log(result)).catch(errorMsg => console.log(errorMsg));
    this.setState({
      sliderViewOptions: false,
    })
  }

  addToAframe = (data) => { 
    if ((this.state.demoAsync !== null) && (this.state.demoAsync !== '') && (JSON.parse(this.state.demoAsync).length > 0)) {
      let arrdemo = [];
      let arrdemoNew = [];
      let convParse = JSON.parse(this.state.demoAsync)
      convParse.map((asyncData, i) => {
        if (asyncData.main.SKU !== data.main.SKU) {
          let pushdemo = arrdemo.push(asyncData)
        }
        let pushdemo = arrdemoNew.push(asyncData)
      })
      let pushdemo1 = arrdemo.push(data)
      AsyncStorage.setItem('forAframeAsyncStorage', JSON.stringify(arrdemo), () => {
        if (arrdemoNew.length === arrdemo.length) {
          alert('Already added')
        } else {
          alert(data.main.ProductName.toUpperCase() + ' Added to A-Frame')

        }
        this.setState({
          demoAsync: JSON.stringify(arrdemo),
          b: this.state.b + 1,
          sliderViewOptions: false,
        });
      });
    } else {
      let arrdemo = [];
      let pushdemo1 = arrdemo.push(data)
      AsyncStorage.setItem('forAframeAsyncStorage', JSON.stringify(arrdemo), () => {
        alert(data.main.ProductName.toUpperCase() + ' Added to A-Frame')
        this.setState({
          demoAsync: JSON.stringify(arrdemo),
          b: this.state.b + 1,
          sliderViewOptions: false,
        });
      });
    }
  }

  warehouseMap = (loc) => {
    return <TouchableOpacity onPress={() => this.openWarehouseMap(loc)}>
      <Text style={styles.colHeaderTextData} >{this.locShowUser(loc)}</Text>
    </TouchableOpacity>
  }

  locShowUser = (loc) => {
    if (loc.toLowerCase() === "fabricators stone group") {
      return "Fabstone McKinney";
    }
    if (loc.toLowerCase() === "fabstone dallas") {
      return "Elements Dallas";
    }
    return "Other";
  }

  openWarehouseMap = (loc) => {
    if (loc.toLowerCase() === "fabricators stone group") {
      Platform.OS === 'ios' ? Linking.openURL("http://maps.apple.com/?ll=33.220303,-96.6171767") : Linking.openURL('geo:33.220303,-96.6171767');
    }
    if (loc.toLowerCase() === "fabstone dallas") {
      Platform.OS === 'ios' ? Linking.openURL("http://maps.apple.com/?ll=32.8894086,-96.8858619") : Linking.openURL('geo:32.8894086,-96.8858619');
    }
  }

  _properCase = (arg) => {
    arg = arg.split(' ');
    arg.map((data, index) => {
      arg[index] = data.charAt(0).toUpperCase() + data.slice(1).toLowerCase();
    });
    return arg.join(' ');
  }

  constructor(props) {
    super(props);
    this.state = {
      navigate: this.props.navigation.navigate,
      sliderViewOptions: false,
      singleProductDataList: [],
      demoAsync: [],
      ProData: [],
      ElementInventoryloginDetailProductList: '0',
      ElementInventoryloginDetailNameProductList: '0',
      isLoading: true,
      InventoryViewSetting: '0',
      singleProMainImage: null,
      userLoginPrice: null,
      b: 0,
      totalOnHand: 0,
      totalAvailable: 0,
      isCompoShow: false,
      BundleImg_for_icon: ""
    }
  }

  componentWillMount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }
  componentWillUnmount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }

  handleBackButtonClick = () => {
    this.props.settingIcon(0)
    this.props.contactIcon(0)
    this.props.searchIcon(1)
    this.props.specialIcon(0)
    this.props.aFrameIcon(0)
  }
  _increaseCount = () => {
    this.props.searchIcon(1);
    this.props.navigation.goBack();
  }


  async componentDidMount() {
    this.props.navigation.setParams({ increaseCount: this._increaseCount });
    AsyncStorage.getItem("ElementInventoryloginUser").then((value) => {
      if (value === null || value === undefined || value === "") {
        this.setState({ userLoginPrice: null });
      } else {
        let temp = JSON.parse(value);
        let loginPrice = temp.meta_data.business_type;
        this.setState({ userLoginPrice: loginPrice })
      }
    }).done();

    AsyncStorage.getItem("ViewInventoryAsSetting").then((value) => {
      this.setState({ "InventoryViewSetting": value })
    }).done();

    AsyncStorage.getItem("ElementInventorylogin").then((value) => {
      value !== null ?
        this.setState({ "ElementInventoryloginDetailProductList": value })
        : this.setState({ "ElementInventoryloginDetailProductList": '0' })
    }).done();

    AsyncStorage.getItem("ElementInventoryloginName").then((value) => {
      value !== null ?
        this.setState({ "ElementInventoryloginDetailNameProductList": value })
        : this.setState({ "ElementInventoryloginDetailNameProductList": '0' })
    }).done();

    AsyncStorage.getItem("forAframeAsyncStorage").then((value) => {
      this.setState({ "demoAsync": value });
    }).done();
    let defaultAvailable = await AsyncStorage.getItem('AvailableInventoryAsSetting');

    let res = await AsyncStorage.getItem('SettingsDefaultWarehouse');
    let ndata = res;

    var that = this
    const { params } = this.props.navigation.state;
    let proSKU = params.ProDataSKU ? params.ProDataSKU : "<undefined>";
    let rfs = await AsyncStorage.getItem('refine_search_warehouse');
    let rfs_av = await AsyncStorage.getItem('refine_search_Available');

    let rfs_available = rfs_av;
    let refine_search_av = '';

    if (rfs_available != '' && rfs_available != null) {
      refine_search_av = rfs_available;
    } else {
      refine_search_av = defaultAvailable;
    }




    let refinSearchWarehouse = rfs;
    let refind_search_screen = '';
    if (refinSearchWarehouse === "Fabstone Dallas") {
      refind_search_screen = 'Fabstone Dallas'
    } else if (refinSearchWarehouse === "Fabricators Stone Group") {
      refind_search_screen = 'Fabricators Stone Group'
    }
    else if (refinSearchWarehouse === "all") {
      refind_search_screen = 'All'
    }
    else {
      refind_search_screen = `${ndata}`
    }
    let finalData = {
      "sku": proSKU,
      "warehouse": refind_search_screen,
      "available": refine_search_av
    }
    axios.post(`${URL.URL_CATEGORY}`, finalData, {})
      .then(function (response) {   
        if (response.data.status === 1) {

          if (response.data.result[0].bundles !== 'Record not found!') {
            let Bundle_Img = response.data.result[0].bundles[0].Bundlefile_Filename
            that.setState({
              BundleImg_for_icon: Bundle_Img
            })
          }

          let totalOnHand = totalAvailable = 0;
          response.data.result[0].bundles !== "Record not found!" && response.data.result[0].bundles.map((val, index) => {
            totalOnHand += Number(val.PCOnHand);
            totalAvailable += Number(val.CurrentAvailabeQty);
          });
          that.setState({ singleProductDataList: response.data.result[0].bundles, isLoading: false, totalOnHand, totalAvailable, ProData: response.data.result[0], singleProMainImage: response.data.result[0].main.item_Filename });
        } else {
          that.setState({ isLoading: false, totalOnHand: 0, singleProductData: [], totalAvailable: 0, ProData: [], singleProMainImage: '', isCompoShow: true })
        }
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    const { ProData, BundleImg_for_icon } = this.state;
    return (
      <Container style={styles.container} >
        {this.state.isCompoShow === true ?
          <View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ fontSize: 20 }}>No Product Found</Text>
            </View>
          </View>
          :

          <View style={{ flex: 1 }}>
            {!this.state.isLoading ?
              <View style={{ flex: 1 }}>
                <Grid style={styles.content} >
                  <Row style={styles.Row1}><H3 style={styles.singleProTitle} numberOfLines={1} >{ProData.main.ProductName.toUpperCase()} </H3></Row>

                  <Row style={styles.Row2}>
                    <Col style={styles.minDetailsProImageCol} size={45}>
                      <Row>
                        <Image
                          style={styles.minDetailsProImage}
                          source={{ uri: this.state.singleProMainImage !== null && this.state.singleProMainImage !== '' ? URL.BaseImgUrl + this.state.singleProMainImage : BundleImg_for_icon === "" ? URL.FABSTON_LOGO : URL.BaseImgUrl + BundleImg_for_icon }}
                          resizeMode={this.state.singleProMainImage !== null && this.state.singleProMainImage !== '' ? null : "contain"}
                        />
                      </Row>
                    </Col>
                    <Col style={styles.colDetail} size={55}>
                      <Row>
                        <Col size={35}>
                          <Text style={styles.detailTitles}  >SKU </Text>
                        </Col>
                        <Col size={65}>
                          <Text numberOfLines={1} style={styles.minDetailsProTextRow} >: {ProData.main.SKU}</Text>
                        </Col>
                      </Row>
                      <Row style={{ paddingTop: 2 }}>
                        <Col size={35}>
                          <Text style={styles.detailTitles}  >Type </Text>
                        </Col>
                        <Col size={65}>
                          <Text numberOfLines={1} style={styles.minDetailsProTextRow} >: {ProData.main.Type === "" ? "N/A" : ProData.main.Type}</Text>
                        </Col>
                      </Row>

                      <Row style={{ paddingTop: 2 }}>
                        <Col size={35}>
                          <Text style={styles.detailTitles}  >Category </Text>
                        </Col>
                        <Col size={65}>
                          <Text numberOfLines={1} style={styles.minDetailsProTextRow} >: {ProData.main.Category === "" ? "N/A" : ProData.main.Category}</Text>
                        </Col>
                      </Row>

                      <Row style={{ paddingTop: 2 }}>
                        <Col size={35}>
                          <Text style={styles.detailTitles}  >Color </Text>
                        </Col>
                        <Col size={65}>
                          <Text numberOfLines={1} style={styles.minDetailsProTextRow} >: {ProData.main.Item_BaseColor === "" ? "N/A" : ProData.main.Item_BaseColor}</Text>
                        </Col>
                      </Row>
                      <Row style={{ paddingTop: 2 }}>
                        <Col size={35}>
                          <Text style={styles.detailTitles} >Series </Text>
                        </Col>
                        <Col size={65}>
                          <Text numberOfLines={1} style={styles.minDetailsProTextRow} >: {this._properCase(ProData.main.ProductGroup)}</Text>
                        </Col>
                      </Row>
                      <Row style={{ paddingTop: 2 }}>
                        <Col size={35}>
                          <Text style={styles.detailTitles} >On-Hand </Text>
                        </Col>
                        <Col size={65}>
                          <Text numberOfLines={1} style={styles.minDetailsProTextRow}  >: {this.state.totalOnHand}</Text>
                        </Col>
                      </Row>
                      <Row style={{ paddingTop: 2 }}>
                        <Col size={35}>
                          <Text style={styles.detailTitles} >Available </Text>
                        </Col>
                        <Col size={65}>
                          <Text numberOfLines={1} style={styles.minDetailsProTextRow}  >: {this.state.totalAvailable}</Text>
                        </Col>
                      </Row>
                    </Col>
                  </Row>

                  {
                    ProData.main.UnitCost && this.state.userLoginPrice === null && (
                      <Row style={styles.priceRow}>
                        <Col>
                          <Text style={styles.priceText} >Price per SqFt: {this.state.ElementInventoryloginDetailProductList !== '0' ? this.precisionRound(ProData.main.UnitCost) : (this.precisionRoundWithoutPoint(ProData.main.UnitCost).replace('$', "")).replace(/[0-9]/g, "$")}</Text>
                        </Col>
                      </Row>
                    )
                  }
                  {
                    this.state.userLoginPrice !== null && ProData.bundles !== "Record not found!" && (
                      <Row style={styles.priceRow}>
                        <Col>
                          <Text style={styles.priceText} >Price per SqFt: {this.state.ElementInventoryloginDetailProductList !== '0' ? this._checkUserCatPrice(ProData.bundles[0]) : (this.precisionRoundWithoutPoint(ProData.bundles[0].UnitCost).replace('$', "")).replace(/[0-9]/g, "$")}</Text>
                        </Col>
                      </Row>
                    )
                  }
                  {this.state.singleProductDataList.length > 0 && this.state.ProData ?
                    (<Row style={styles.Row3}>
                      <Col style={styles.colHeader} size={15}>
                        <Text style={styles.colHeaderText} >Bundle</Text>
                      </Col>
                      <Col style={styles.colHeader} size={20}>
                        <Text style={styles.colHeaderText} >Avg Size</Text>
                      </Col>
                      <Col style={styles.colHeader} size={20}>
                        <Text style={styles.colHeaderText} >On Hand</Text>
                      </Col>
                      <Col style={styles.colHeader} size={20}>
                        <Text style={styles.colHeaderText} >Available</Text>
                      </Col>
                      <Col style={styles.colHeader} size={25}>
                        <Text style={styles.colHeaderText} >Location</Text>
                      </Col>
                    </Row>) : null
                  }
                  <Content>
                    {this.state.isLoading ?
                      (<View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 100 }}>
                        <ActivityIndicator size="large" color="#323232" />
                      </View>) : (
                        this.state.ProData.bundles !== "Record not found!" ? <View>
                          {
                            this.state.singleProductDataList.length > 0 ? (
                              this.state.singleProductDataList.map((data, i) => {
                                return (<Row key={i}>
                                  <Col style={styles.colHeader} size={20}>
                                    <Text style={styles.colHeaderTextData} >{data.Bundle}</Text>
                                  </Col>
                                  <Col style={styles.colHeader} size={20}>
                                    {/* <Text style={styles.colHeaderTextData} >{Math.round(data.AvgLength * 100) / 100}" x {Math.round(data.AvgWidth * 100) / 100}"</Text> */}
                                    <Text style={styles.colHeaderTextData} >"{data.Dimensions}"</Text>

                                  </Col>
                                  <Col style={styles.colHeader} size={20}>
                                    <Text style={styles.colHeaderTextData} >{data.PCOnHand} slab(s)</Text>
                                  </Col>
                                  <Col style={styles.colHeader} size={20}>
                                    <Text style={styles.colHeaderTextData}  >{data.CurrentAvailabeQty} slab(s)</Text>
                                  </Col>
                                  <Col style={styles.colHeader} size={25}>
                                    {this.warehouseMap(data.LocationName)}
                                  </Col>
                                </Row>

                                )
                              })
                            ) : null
                          }
                        </View> : <View style={{ paddingTop: 25 }}>
                            <Text style={{ textAlign: 'center', fontWeight: '600' }}>No individual slabs found for this SKU</Text>
                          </View>
                      )
                    }
                  </Content>
                </Grid>
                {this.state.sliderViewOptions ? <View style={styles.zViewPopUpMain}>
                  <View style={styles.zSubViewPopUp} >
                    <Button
                      block transparent
                      style={styles.zSubViewPopUpButton}
                      onPress={() => this.showSliderView()}
                    >
                      <Image
                        style={styles.zSubViewPopUpImgIcon}
                        resizeMode="contain"
                        source={require('../../assets/view-photos.png')}
                      />
                      <Text style={styles.zSubViewPopUpText} >View as Photos</Text>
                    </Button>
                  </View>
                  <View style={styles.zSubViewPopUp} >
                    <Button block transparent style={styles.zSubViewPopUpButton} onPress={() => this.addToAframe(ProData)}>
                      <Image
                        style={styles.zSubViewPopUpImgIcon}
                        resizeMode="contain"
                        source={require('../../assets/a-icon.png')}
                      />
                      <Text style={styles.zSubViewPopUpText} >Add to A-Frame</Text>
                    </Button>
                  </View>
                  <View style={styles.zSubViewPopUp} >
                    <Button block transparent style={styles.zSubViewPopUpButton} onPress={() => this.enlargeImage(this.state.singleProMainImage !== '' && this.state.singleProMainImage !== null ? URL.BaseImgUrl + this.state.singleProMainImage : URL.FABSTON_LOGO)}>
                      <Image
                        style={styles.zSubViewPopUpImgIcon}
                        resizeMode="contain"
                        source={require('../../assets/viewLarger.png')}
                      />
                      <Text style={styles.zSubViewPopUpText} >View Larger Photo</Text>
                    </Button>
                  </View>
                  <View style={styles.zSubViewPopUp} >
                    <Button block transparent style={styles.zSubViewPopUpButton} onPress={() => this.shareProductSlide(ProData)}>
                      <Image
                        style={styles.zSubViewPopUpImgIcon}
                        resizeMode="contain"
                        source={require('../../assets/share-bundle.png')}
                      />
                      <Text style={styles.zSubViewPopUpText} >Share Bundle</Text>
                    </Button>
                  </View>
                  <View style={styles.zSubViewPopUpCancle} >
                    <Button block transparent
                      style={styles.zSubViewPopUpButton_1}
                      onPress={() => this.setState({ sliderViewOptions: false, })}
                    >
                      <Text style={styles.zSubViewPopUpTextCancle} >Cancel</Text>
                    </Button>
                  </View>
                </View> : <View style={styles.viewHeight}>
                    <View style={styles.view3}>
                      <View style={styles.view3Inner1}>
                        <Button transparent
                          onPress={() => this.props.navigation.goBack()}
                        >
                          <Image
                            style={styles.view3InnerImage}
                            source={require('../../assets/backArrow.png')}
                            resizeMode="contain"
                          />
                        </Button>
                      </View>
                      <View style={styles.view3Inner2}>
                        <View>
                          <Button transparent
                            onPress={() => this.optionsWithSlider()}
                          >
                            <Text style={styles.actionDots}>ACTIONS</Text>
                          </Button>
                        </View>
                      </View>
                    </View>
                  </View>}
              </View> : <View style={{ flex: 1 }}>
                <View style={{ justifyContent: "center", alignContent: "center", alignItems: "center" }}>
                  <ActivityIndicator size="large" color="#323232" />
                </View>
              </View>
            }
          </View>

        }
        <Footer navigation={this.props.navigation} b={this.state.b} />
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    footerTabChangee: state.footerTabChange
  }
}
function mapDispatchToProps(dispatch) {
  return {
    settingIcon: (text) => dispatch(settingIcon(text)),
    contactIcon: (text) => dispatch(contactIcon(text)),
    searchIcon: (text) => dispatch(searchIcon(text)),
    specialIcon: (text) => dispatch(specialIcon(text)),
    aFrameIcon: (text) => dispatch(aFrameIcon(text)),
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CollectionProducts)