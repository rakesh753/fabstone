
import React, { Component } from 'react';
import { Image, Text, View, AsyncStorage, BackHandler, Share, ScrollView, ActivityIndicator, Dimensions, TouchableOpacity, Platform, Linking, YellowBox } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { Button, H3, Icon, Container, Content } from 'native-base';
import styles from "./singleProductSliderStyle";
import Footer from "../../components/footer/index";
import * as URL from "../../constants/constants";
import axios from "axios";
import { connect } from 'react-redux';
import { settingIcon, contactIcon, searchIcon, specialIcon, aFrameIcon } from "../../actions/index";





const deviceWidth = Dimensions.get("window").width;
const B = (props) => <Text style={{ fontWeight: '600' }}>{props.children}</Text>
const adjustsFontSizeToFit = (window.width >= 375 ? false : true);

String.prototype.capitalize = function () {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.toProperCase = function () {
	return this.toLowerCase().replace(/^(.)|\s(.)/g,
		function ($1) { return $1.toUpperCase(); });
}

class CollectionProducts extends Component {

	static navigationOptions = ({ navigation }) => ({
		title: "Material Details",
		headerLeft: <Icon
			name='arrow-back'
			onPress={() => navigation.state.params.increaseCount()}
			style={{ color: '#fff', marginLeft: 10 }}
		/>
	});

	enlargeImage = (img) => {
		this.setState({
			listViewOptions: false,
		})
		this.state.navigate('EnlargeImage', { img: img })
	}

	shareProductSlide = (data) => {
		let msgString = "Name: " + data.main.ProductName.toUpperCase() + "\n" + "Type: " + data.main.Category + "\n" + "Series: " + data.main.ProductGroup + "\n" + "SKU: " + data.main.SKU + "\n\n";

		let val = data.bundles[this.state.pgNum];
		if (data.bundles !== "Record not found!") {
			msgString += "Bundle Number: " + val.Bundle + "\nBlock Number: " + val.Block + "\nAvg Size: "
				+ val.Dimensions +
				"\" \n" + "On Hand: " + val.PCOnHand + "slab(s)" + "\nAvailable: " + val.CurrentAvailabeQty + "slab(s)" + "\nLocation: " + this.locShowUser(val.LocationName) + "\n\n";
		}
		let ImgUrl = ''
		if (data.main.item_Filename !== "" && data.main.item_Filename !== null) {
			ImgUrl = URL.BaseImgUrl + data.main.item_Filename;
		} else {
			ImgUrl = URL.FABSTON_LOGO;
		}
		msgString += ImgUrl;

		Share.share(
			{
				message: msgString,			
			}).then(result => console.log(result)).catch(errorMsg => console.log(errorMsg));
		this.setState({
			listViewOptions: false,
		})
	}

	optionsWithList = () => {
		this.setState({ listViewOptions: true, })
	}

	showListView = () => {
		let { state } = this.props.navigation;
		let ProDataList = state.params ? JSON.parse(state.params.ProData) : "<undefined>";
		let ProDataSKU = state.params ? state.params.ProDataSKU : "<undefined>";
		this.setState({ listViewOptions: false, })
		this.props.navigation.goBack()
		this.state.navigate("SingleProductList", { ProData: JSON.stringify(ProDataList), ProDataSKU: ProDataSKU })
	}

	precisionRound = (number) => {
		let no = number.replace("$", "");
		return `$${+(Math.round(no * 100) / 100)}`;
	}

	precisionRoundWithoutPoint = (number) => {
		let no = number.replace("$", "");
		return `$${+(Math.round(Math.trunc(no) * 100) / 100)}`;
	}

	_checkUserCatPrice = (item) => {
		const { userLoginPrice } = this.state;
		let switchKey = userLoginPrice.toLowerCase();

		switch (switchKey) {
			case "contractor":
				return this.precisionRound(item.ConPrice);
				break;

			case "designer":
				return this.precisionRound(item.DesPrice);
				break;

			case "fabricator":
				return this.precisionRound(item.FabPrice);
				break;

			case "Fabricatorb":
				return this.precisionRound(item.FabPrice);
				break;

			case "retail":
				return this.precisionRound(item.RetPrice);
				break;

			default:
				return this.precisionRound(item.UnitCost);
				break;
		}
	}

	_properCase = (arg) => {
		arg = arg.split(' ');
		arg.map((data, index) => {
			arg[index] = data.charAt(0).toUpperCase() + data.slice(1).toLowerCase();
		});
		return arg.join(' ');
	}

	componentWillMount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }
	componentWillUnmount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick); }

	handleBackButtonClick = () => {
		this.props.settingIcon(0)
		this.props.contactIcon(0)
		this.props.searchIcon(1)
		this.props.specialIcon(0)
		this.props.aFrameIcon(0)
	}
	_increaseCount = () => {
		this.props.searchIcon(1);
		this.props.navigation.goBack();
	}

	async componentDidMount() {
		this.props.navigation.setParams({ increaseCount: this._increaseCount });

		AsyncStorage.getItem("ElementInventoryloginUser").then((value) => {
			if (value === null || value === undefined || value === "") {
				this.setState({ userLoginPrice: null });
			} else {
				let temp = JSON.parse(value);
				let loginPrice = temp.meta_data.business_type;
				this.setState({ userLoginPrice: loginPrice })
			}
		}).done();

		AsyncStorage.getItem("ElementInventorylogin").then((value) => {
			value !== null ?
				this.setState({ "ElementInventoryloginDetailProductSlider": value })
				: this.setState({ "ElementInventoryloginDetailProductSlider": '0' })
		}).done();
		AsyncStorage.getItem("ElementInventoryloginName").then((value) => {
			value !== null ?
				this.setState({ "ElementInventoryloginDetailNameProductSlider": value })
				: this.setState({ "ElementInventoryloginDetailNameProductSlider": '0' })
		}).done();
		AsyncStorage.getItem("forAframeAsyncStorage").then((value) => {
			this.setState({ "demoAsync": value });
		}).done();


		let res = await AsyncStorage.getItem('SettingsDefaultWarehouse');
		let ndata = res;
		var that = this
		const { params } = this.props.navigation.state;
		let categorySKU = params ? params.ProDataSKU : null;

		let rfs = await AsyncStorage.getItem('refine_search_warehouse');

		let rfs_av = await AsyncStorage.getItem('refine_search_Available');

		let defaultAvailable = await AsyncStorage.getItem('AvailableInventoryAsSetting');

		let rfs_available = rfs_av;
		let refine_search_av = '';

		let refinSearchWarehouse = rfs;
		let refind_search_screen = '';

		if (rfs_available != '' && rfs_available != null) {
			refine_search_av = rfs_available;
		} else {
			refine_search_av = defaultAvailable;
		}

		if (refinSearchWarehouse === "Fabstone Dallas") {
			refind_search_screen = 'Fabstone Dallas'
		} else if (refinSearchWarehouse === "Fabricators Stone Group") {
			refind_search_screen = 'Fabricators Stone Group'
		}
		else if (refinSearchWarehouse === "all") {
			refind_search_screen = 'All'
		}
		else {
			refind_search_screen = `${ndata}`
		}

		let finalData = {
			"sku": categorySKU,
			"warehouse": refind_search_screen,
			"available": refine_search_av
		}

		axios
			.post(`${URL.URL_CATEGORY}`, finalData, {

			})
			.then(function (response) {			
				if (response.data.status === 1) {

					let totalOnHand = totalAvailable = 0;
					if (response.data.result[0].bundles !== 'Record not found!') {
						let Bundle_Img = response.data.result[0].bundles[0].Bundlefile_Filename
						that.setState({
							BundleImg_for_icon: Bundle_Img
						})
					}
					response.data.result[0].bundles !== 'Record not found!' && response.data.result[0].bundles.map((val, index) => {
						totalOnHand += Number(val.PCOnHand);
						totalAvailable += Number(val.CurrentAvailabeQty);
					});

					that.setState({ singleProductData: response.data.result[0].bundles, isLoading: false, totalOnHand, totalAvailable, ProData: response.data.result[0], singleProMainImage: response.data.result[0].main.item_Filename });
				} else {
					that.setState({ isLoading: false, totalOnHand: 0, singleProductData: [], totalAvailable: 0, ProData: [], singleProMainImage: '', isCompoShow: true })
				}
			})
			.catch(function (error) {
				that.setState({ isLoading: false, })

				console.log(error);
			});
	}
	addToAframe = (data) => {

		if ((this.state.demoAsync !== null) && (this.state.demoAsync !== '') && (JSON.parse(this.state.demoAsync).length > 0)) {
			let arrdemo = [];
			let arrdemoNew = [];
			let convParse = JSON.parse(this.state.demoAsync)
			convParse.map((asyncData, i) => {
				if (asyncData.main.SKU !== data.main.SKU) {
					let pushdemo = arrdemo.push(asyncData)
				}
				let pushdemo = arrdemoNew.push(asyncData)
			})
			let pushdemo1 = arrdemo.push(data)
			AsyncStorage.setItem('forAframeAsyncStorage', JSON.stringify(arrdemo), () => {
				if (arrdemoNew.length === arrdemo.length) {
					alert('Already added')
				} else {
					alert(data.main.ProductName.toUpperCase() + ' Added to A-Frame')
				}
				this.setState({
					demoAsync: JSON.stringify(arrdemo),
					a: this.state.a + 1,
					listViewOptions: false,
				});
			});
		} else {
			let arrdemo = [];
			let pushdemo1 = arrdemo.push(data)
			AsyncStorage.setItem('forAframeAsyncStorage', JSON.stringify(arrdemo), () => {
				alert(data.main.ProductName.toUpperCase() + ' Added to A-Frame')
				this.setState({
					demoAsync: JSON.stringify(arrdemo),
					a: this.state.a + 1,
					listViewOptions: false,
				});
			});
		}


	}

	warehouseMap = (loc) => {
		return <Text style={styles.cardItemDetailsText} ><B>Location:</B> <Text onPress={() => this.openWarehouseMap(loc)} style={styles.cardItemDetailsText} >{this.locShowUser(loc)}</Text></Text>
	}

	locShowUser = (loc) => {
		if (loc.toLowerCase() === "fabricators stone group") {
			return "Fabstone McKinney";
		}
		if (loc.toLowerCase() === "fabstone dallas") {
			return "Elements Dallas";
		}
		return "Other";
	}

	openWarehouseMap = (loc) => {
		if (loc.toLowerCase() === "fabricators stone group") {
			Platform.OS === 'ios' ? Linking.openURL("http://maps.apple.com/?ll=33.220303,-96.6171767") : Linking.openURL('geo:33.220303,-96.6171767');
		}
		if (loc.toLowerCase() === "fabstone dallas") {
			Platform.OS === 'ios' ? Linking.openURL("http://maps.apple.com/?ll=32.8894086,-96.8858619") : Linking.openURL('geo:32.8894086,-96.8858619');
		}
	}

	constructor(props) {
		super(props);
		this.state = {
			listViewOptions: false,
			navigate: this.props.navigation.navigate,
			demoAsync: [],
			ProData: [],
			singleProductData: [],
			isLoading: true,
			ElementInventoryloginDetailProductSlider: '0',
			ElementInventoryloginDetailNameProductSlider: '0',
			singleProMainData: [],
			singleProMainImage: null,
			userLoginPrice: null,
			a: 0,
			pgNum: 0,
			totalOnHand: 0,
			totalAvailable: 0,
			filterByLocation: '',
			isCompoShow: false,
			BundleImg_for_icon: ""
		}
	}

	render() {
		const { ProData, BundleImg_for_icon } = this.state;
		let dataVariable = this.state.singleProductData;
		// console.log('BundleImg_for_icon', BundleImg_for_icon)
		return (
			<Container style={styles.container} >
				{this.state.isCompoShow === true ?
					<Content>
						<View style={{ justifyContent: 'center', alignItems: 'center' }}>
							<Text style={{ fontSize: 20 }}>No Product Found</Text>
						</View>
					</Content>
					:
					<View style={{ flex: 1 }}>
						{!this.state.isLoading ?
							<View style={{ flex: 1 }}>
								<ScrollView>
									<Grid style={styles.contentGrid} >
										<Row style={styles.Row1}><H3 style={styles.singleProTitle} numberOfLines={1}>{ProData.main.ProductName.toUpperCase()} </H3></Row>
										<Row style={styles.Row2}>
											<Col style={styles.minDetailsProImageCol} size={45}>
												<Row>
													<TouchableOpacity onPress={() => this.enlargeImage(this.state.singleProMainImage !== null && this.state.singleProMainImage !== '' ? URL.BaseImgUrl + this.state.singleProMainImage : BundleImg_for_icon === "" ? URL.FABSTON_LOGO : URL.BaseImgUrl + BundleImg_for_icon)} >
														<Image
															style={styles.minDetailsProImage}
															source={{ uri: this.state.singleProMainImage !== null && this.state.singleProMainImage !== '' ? URL.BaseImgUrl + this.state.singleProMainImage : BundleImg_for_icon === "" ? URL.FABSTON_LOGO : URL.BaseImgUrl + BundleImg_for_icon }}
															resizeMode={this.state.singleProMainImage !== null && this.state.singleProMainImage !== '' ? null : "contain"}
														/>
													</TouchableOpacity>
												</Row>
											</Col>
											<Col style={styles.colDetail} size={55}>
												<Row>
													<Col size={35}>
														<Text style={styles.detailTitles}  >SKU</Text>
													</Col>
													<Col size={65}>
														<Text numberOfLines={1} style={styles.minDetailsProTextRow} >: {ProData.main.SKU}</Text>
													</Col>
												</Row>
												<Row style={{ paddingTop: 2 }}>
													<Col size={35}>
														<Text style={styles.detailTitles}  >Type</Text>
													</Col>
													<Col size={65}>
														<Text numberOfLines={1} style={styles.minDetailsProTextRow} >: {ProData.main.Type === "" ? "N/A" : ProData.main.Type}</Text>
													</Col>
												</Row>

												<Row style={{ paddingTop: 2 }}>
													<Col size={35}>
														<Text style={styles.detailTitles}  >Category</Text>
													</Col>
													<Col size={65}>
														<Text numberOfLines={1} style={styles.minDetailsProTextRow} >: {ProData.main.Category === "" ? "N/A" : ProData.main.Category}</Text>
													</Col>
												</Row>


												<Row style={{ paddingTop: 2 }}>
													<Col size={35}>
														<Text style={styles.detailTitles}  >Color</Text>
													</Col>
													<Col size={65}>
														<Text numberOfLines={1} style={styles.minDetailsProTextRow} >: {ProData.main.Item_BaseColor === "" ? "N/A" : ProData.main.Item_BaseColor}</Text>
													</Col>
												</Row>
												<Row style={{ paddingTop: 2 }}>
													<Col size={35}>
														<Text style={styles.detailTitles} >Series</Text>
													</Col>
													<Col size={65}>
														<Text numberOfLines={1} style={styles.minDetailsProTextRow}  >: {this._properCase(ProData.main.ProductGroup)}</Text>
													</Col>
												</Row>
												<Row style={{ paddingTop: 2 }}>
													<Col size={35}>
														<Text style={styles.detailTitles} >On-Hand</Text>
													</Col>
													<Col size={65}>
														<Text numberOfLines={1} style={styles.minDetailsProTextRow}  >: {this.state.totalOnHand}</Text>
													</Col>
												</Row>
												<Row style={{ paddingTop: 2 }}>
													<Col size={35}>
														<Text style={styles.detailTitles} >Available</Text>
													</Col>
													<Col size={65}>
														<Text numberOfLines={1} style={styles.minDetailsProTextRow}  >: {this.state.totalAvailable}</Text>
													</Col>
												</Row>
											</Col>
										</Row>

										{
											ProData.main.UnitCost && this.state.userLoginPrice === null && (
												<Row style={styles.priceRow}>
													<Col>
														<Text style={styles.priceText}>Price per SqFt: {this.state.ElementInventoryloginDetailProductSlider !== '0' ? this.precisionRound(ProData.main.UnitCost) : (this.precisionRoundWithoutPoint(ProData.main.UnitCost).replace('$', "")).replace(/[0-9]/g, "$")}</Text>
													</Col>
												</Row>
											)
										}
										{
											this.state.userLoginPrice !== null && ProData.bundles !== "Record not found!" && (
												<Row style={styles.priceRow}>
													<Col>
														<Text style={styles.priceText}>Price per SqFt: {this.state.ElementInventoryloginDetailProductSlider !== '0' ? this._checkUserCatPrice(ProData.bundles[0]) : (this.precisionRoundWithoutPoint(ProData.bundles[0].UnitCost).replace('$', "")).replace(/[0-9]/g, "$")}</Text>
													</Col>
												</Row>
											)
										}
										<Row >
											<View style={styles.view1} >
												{this.state.isLoading ?
													(<View style={{ flex: 1, justifyContent: 'center' }}>
														<ActivityIndicator size="large" color="#323232" />
													</View>) :
													(this.state.ProData.bundles !== "Record not found!"
														?
														<View>
															<ScrollView horizontal={true} pagingEnabled={true} showsHorizontalScrollIndicator={false}
																onMomentumScrollEnd={(event) => {
																	var nextx = event.nativeEvent.contentOffset.x;
																	this.setState({
																		pgNum: Math.round((nextx / (deviceWidth - 20)))
																	})
																}}
															>
																<View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', marginBottom: 15 }}>
																	{this.state.singleProductData.length > 0 ? (
																		this.state.singleProductData.map((data, i) => {

																			let sliderImg = data.Bundlefile_Filename !== "" && data.Bundlefile_Filename !== null && data.Bundlefile_Filename !== undefined ? URL.BaseImgUrl + data.Bundlefile_Filename : URL.FABSTON_LOGO;
																			return (
																				<View key={i} style={[{ width: deviceWidth - 20, }]}>
																					<View style={{ width: deviceWidth - 20, }}>
																						<TouchableOpacity onPress={() => this.enlargeImage(sliderImg)}>
																							<Image
																								style={styles.cardImage}
																								source={{ uri: sliderImg }}
																								resizeMode={'stretch'}
																							/>
																						</TouchableOpacity>
																					</View>
																					<View style={{ flexDirection: 'row', width: deviceWidth - 20, paddingTop: 15, paddingLeft: 2, flexWrap: 'wrap' }}>
																						<View style={{ flexDirection: 'column', width: '60%' }}>
																							<Text style={styles.cardItemDetailsText} ><B>Block:</B> {data.Block === "" ? 0 : data.Block}</Text>
																							<Text style={styles.cardItemDetailsText} ><B>Bundle:</B> {data.Bundle}</Text>
																							{this.warehouseMap(data.LocationName)}
																						</View>
																						<View style={{ flexDirection: 'column', width: '40%' }}>
																							<Text style={styles.cardItemDetailsText} ><B>On-Hand:</B> {data.PCOnHand} slab(s)</Text>
																							<Text style={styles.cardItemDetailsText} ><B>Available:</B> {data.CurrentAvailabeQty}</Text>
																							<Text style={styles.cardItemDetailsText} ><B>Avg Size:</B>
																								"{data.Dimensions}"
																							{/* {Math.round(data.AvgLength * 100) / 100}"x{Math.round(data.AvgWidth * 100) / 100}" */}
																							</Text>
																						</View>
																					</View>

																				</View>
																			)
																		})
																	) : null
																	}
																</View>
															</ScrollView>
															<View style={{ width: deviceWidth - 20, justifyContent: 'center', alignContent: 'center', alignItems: 'center', flexDirection: 'row', flexWrap: 'wrap' }}>
																{
																	this.state.singleProductData.map((data, index) => {
																		return <View style={[{ height: 5, width: ((deviceWidth - 25) / this.state.singleProductData.length), borderRadius: 5, borderColor: '#E5E5E5', borderWidth: 1, }, index === this.state.pgNum ? { backgroundColor: '#E5E5E5' } : {}]} key={index} />
																	})
																}
															</View>
														</View>
														:
														<View style={{ paddingTop: 25 }}>
															<Text style={{ textAlign: 'center', fontWeight: '600' }}>No individual slabs found for this SKU</Text>
														</View>
													)}
											</View>
										</Row>

									</Grid>
								</ScrollView>
								<View style={styles.viewHeight}>
									<View style={styles.view3}>
										<View style={styles.view3Inner1}>
											<Button transparent
												onPress={() => this.props.navigation.goBack()}
											>
												<Image
													style={styles.view3InnerImage}
													source={require('../../assets/backArrow.png')}
													resizeMode="contain"
												/>
											</Button>
										</View>
										<View style={styles.view3Inner2}>
											<View>
												<Button transparent
													onPress={() => this.optionsWithList()}
												>
													<Text style={styles.actionDots}>ACTIONS</Text>
												</Button>
											</View>
										</View>
									</View>
								</View>

								{this.state.listViewOptions ? <View style={styles.zListViewOptions}>
									<View style={styles.zListViewOptions_1}>
										<Button transparent
											block
											style={styles.zListViewOptions_1_Button}
											onPress={() => this.showListView()}
										>
											<Image
												style={styles.zListViewOptions_1_ImagIcon}
												resizeMode="contain"
												source={require('../../assets/List.png')}
											/>
											<Text style={styles.zListViewOptions_1_Text} >View as a List</Text>
										</Button>
									</View>
									<View style={styles.zListViewOptions_1} >
										<Button block style={styles.zListViewOptions_1_Button} transparent onPress={() => this.addToAframe(ProData)}>
											<Image
												style={styles.zListViewOptions_1_ImagIcon}
												resizeMode="contain"
												source={require('../../assets/a-icon.png')}
											/>
											<Text style={styles.zListViewOptions_1_Text} >Add to A-Frame</Text>
										</Button>
									</View>

									<View style={styles.zListViewOptions_1} >
										<Button block style={styles.zListViewOptions_1_Button} transparent onPress={() => this.enlargeImage(this.state.singleProMainImage !== null && this.state.singleProMainImage !== '' ? URL.BaseImgUrl + this.state.singleProMainImage : URL.FABSTON_LOGO)}>
											<Image
												style={styles.zListViewOptions_1_ImagIcon}
												resizeMode="contain"
												source={require('../../assets/viewLarger.png')}
											/>
											<Text style={styles.zListViewOptions_1_Text} >View Larger Photo</Text>
										</Button>
									</View>
									<View style={styles.zListViewOptions_1} >
										<Button block style={styles.zListViewOptions_1_Button} transparent onPress={() => this.shareProductSlide(ProData)}>
											<Image
												style={styles.zListViewOptions_1_ImagIcon}
												resizeMode="contain"
												source={require('../../assets/share-bundle.png')}
											/>
											<Text style={styles.zListViewOptions_1_Text} >Share Bundle</Text>
										</Button>
									</View>
									<View style={styles.zListViewOptions_1_cancle} >
										<Button block transparent
											style={styles.zListViewOptions_1_Button_1}
											onPress={() => this.setState({ listViewOptions: false, })}
										>
											<Text style={styles.zListViewOptions_1_Text_1} >Cancel</Text>
										</Button>
									</View>
								</View> : null}
							</View> : <View style={{ flex: 1 }}>
								<View style={{ justifyContent: "center", alignContent: "center", alignItems: "center" }}>
									<ActivityIndicator size="large" color="#323232" />
								</View>
							</View>
						}
					</View>

				}
				<Footer navigation={this.props.navigation} a={this.state.a} />

			</Container>
		);
	}
}

function mapStateToProps(state) {
	return {
		footerTabChangee: state.footerTabChange
	}
}
function mapDispatchToProps(dispatch) {
	return {
		settingIcon: (text) => dispatch(settingIcon(text)),
		contactIcon: (text) => dispatch(contactIcon(text)),
		searchIcon: (text) => dispatch(searchIcon(text)),
		specialIcon: (text) => dispatch(specialIcon(text)),
		aFrameIcon: (text) => dispatch(aFrameIcon(text)),
	}
}
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CollectionProducts)